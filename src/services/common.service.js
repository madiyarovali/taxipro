const url = 'https://backend.yataxi.kz';
const on_mock_server = true;

const common = {
    url,
    on_mock_server
};

export default common;