import AsyncStorage from '@react-native-community/async-storage';
import common from './common.service';

const getDebitCards = async () => {
    var token = await AsyncStorage.getItem('token');
    token = JSON.parse(token);

    var headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token.access_token}`
    };
    var method = 'GET';

    var result = await fetch(`${common.url}/api/credit-cards`, {
        headers,
        method,
    })
    .then(async data => {
        let result = await data.json();
        return result;
    })
    .catch(e => {
        console.log(e);
    });
    if (result.length < 5) {
        result.push({
            text: 'Добавить новую карту',
        });
    }

    return result;
}

const saveDebitCard = async (pan) => {
    var token = await AsyncStorage.getItem('token');
    token = JSON.parse(token);

    var headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token.access_token}`
    };
    var body = JSON.stringify({
        pan
    });
    var method = 'POST';

    var result = await fetch(`${common.url}/api/credit-cards`, {
        headers,
        body,
        method,
    })
    .then(async data => {
        let result = await data.json();
        // console.log(result);
        return result;
    })
    .catch(e => {
        console.log(e);
    });
    // console.log(result);
    return result;
}

const deleteDebitCard = async (card_id) => {
    var token = await AsyncStorage.getItem('token');
    token = JSON.parse(token);

    var headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token.access_token}`
    };
    var method = 'DELETE';

    var result = await fetch(`${common.url}/api/credit-cards/${card_id}`, {
        headers,
        method,
    })
    .then(async data => {
        let result = data.json();
        return result;
    })
    .catch(e => {
        console.log(e);
    });

    return result;
}

const getHistory = async (page = 1) => {
    var token = await AsyncStorage.getItem('token');
    token = JSON.parse(token);

    var headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token.access_token}`
    };
    
    var method = 'GET';

    var result = await fetch(`${common.url}/api/withdraws?page=${page}`, {
        headers,
        method,
    })
    .then(async data => {
        let result = await data.json();
        // console.log(result);
        return result;
    })
    .catch(e => {
        console.log(e);
    });

    return result;
}

const getHistoryPagesCount = async () => {
    var history = await getHistory();
    return history.total;
}

const proceed = async ({ type, card_id }, amount, operation, account_id, current_version) => {
    var token = await AsyncStorage.getItem('token');
    token = JSON.parse(token);

    var headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token.access_token}`
    };
    var body = JSON.stringify({
        card_id, amount, account_id, type, current_version
    });
    var method = 'POST';
    // console.log(operation, body);
    var result = await fetch(`${common.url}/api/${operation}`, {
        headers,
        body,
        method,
    })
    .then(async data => {
        let result = await data.json();
        return result;
    })
    .catch(e => {
        console.log(e);
    });
    return result;
}

const cashout = {
    getDebitCards,
    saveDebitCard,
    deleteDebitCard,
    getHistory,
    getHistoryPagesCount,
    proceed
};

export default cashout;