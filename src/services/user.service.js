import AsyncStorage from '@react-native-community/async-storage';
import common from './common.service';
import DeviceInfo from 'react-native-device-info';

const sendCode = async (phone) => {
    phone = updatePhone(phone);
    await AsyncStorage.removeItem('phone_verified');
    await AsyncStorage.removeItem('pin');

    var headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    };
    var body = JSON.stringify({
        phone
    });
    var method = 'POST';

    var result = await fetch(`${common.url}/api/auth/otp-generate`, {
        headers,
        method,
        body
    })
    .then(async data => {
        let result = await data.json();
        console.log(data.status);
        if (result.hasOwnProperty('exception')) {
            throw result;
        }
        return result;
    })
    .catch(e => {
        console.log(e);
        return e;
    });
    return result;
}

const checkCode = async (phone, password, device_name) => {
    phone = updatePhone(phone);

    var headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    };
    var body = JSON.stringify({
        phone, password, device_name
    });
    var method = 'POST';
    var result = await fetch(`${common.url}/api/auth/login`, {
        headers,
        method,
        body
    })
    .then(async data => {
        let result = await data.json();
        AsyncStorage.setItem('token', JSON.stringify(result));
        return result;
    })
    .catch(e => {
        console.log(e);
    });

    return result;
}

const saveUser = async (phone) => {
    await AsyncStorage.setItem('phone', phone);
    await AsyncStorage.setItem('phone_verified', 'true');
    return;
}

const removeUser = async () => {
    await AsyncStorage.removeItem('phone');
    await AsyncStorage.removeItem('phone_verified');
    await AsyncStorage.removeItem('pin');

    var token = await AsyncStorage.getItem('token');
    token = JSON.parse(token);
    if (token) {
        var headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token.access_token}`
        };
        var method = 'POST';
    
        var result = await fetch(`${common.url}/api/auth/logout`, {
            headers,
            method,
        })
        .then(async data => {
            let result = await data.json();
            await AsyncStorage.removeItem('token');
            return result;
        })
        .catch(e => {
            console.log(e);
        });
    }
}

const userExists = async () => {
    var phone = await AsyncStorage.getItem('phone');
    var phone_verified = await AsyncStorage.getItem('phone_verified');
    return typeof phone == 'string' && typeof phone_verified == 'string' && phone_verified == 'true';
}

const getUser = async () => {
    var phone = await AsyncStorage.getItem('phone');
    return phone;
}

const setPin = async (pin) => {
    await AsyncStorage.setItem('pin', pin);
}

const pinExists = async () => {
    var pin = await AsyncStorage.getItem('pin');
    return typeof pin == 'string';
}

const checkPin = async (pin) => {
    if (common.on_mock_server) {
        return pin == await AsyncStorage.getItem('pin');
    }

    return pin == await AsyncStorage.getItem('pin');;
}

const updatePhone = (phone) => {
    var pattern = /\D+/g;
    phone = phone.replace(pattern, '').substring(1);
    return phone;
}

const sendRequest = async (phone, name) => {
    var headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    };
    var method = 'POST';
    var body = JSON.stringify({
        phone, name
    });
    var result = await fetch(`${common.url}/api/auth/register`, {
        headers,
        body,
        method,
    })
    .then(async data => {
        let result = await data.json();
        return result;
    })
    .catch(e => {
        console.log(e);
    });
    return result;
}

const user = {
    sendCode,
    checkCode,

    saveUser,
    removeUser,
    
    userExists,
    getUser,
    
    setPin,
    pinExists,
    checkPin,
    sendRequest
};

export default user;