import AsyncStorage from '@react-native-community/async-storage';
import common from './common.service';

const userExists = async () => {
    var user_json = await AsyncStorage.getItem('user');
    return typeof user_json == 'string';
}

const getGroups = async () => {
    var token = await AsyncStorage.getItem('token');
    token = JSON.parse(token);

    var headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token.access_token}`
    };
    var method = 'GET';
    var result = await fetch(`${common.url}/api/ranking/groups`, {
        headers,
        method,
    })
    .then(async data => {
        let result = await data.json();
        return result;
    })
    .catch(e => {
        console.log(e);
    });
    return result;
}

const setUser = async () => {
    var token = await AsyncStorage.getItem('token');
    token = JSON.parse(token);

    var headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token.access_token}`
    };
    var method = 'POST';

    var result = await fetch(`${common.url}/api/auth/me`, {
        headers,
        method,
    })
    .then(async data => {
        let result = await data.json();
        let old_user = await getUser();
        result.avatar = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8ODRAPDg4NEA0TDQ0QFg8NEA8NDhUOFREYFxURFRUYHSsiGBolGxYYLTEhJTArLi4uGSAzODMtNygtLiwBCgoKBQUFDgUFDisZExkrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAOEA4QMBIgACEQEDEQH/xAAcAAEAAgMBAQEAAAAAAAAAAAAAAQcFBggEAwL/xABBEAACAgEBAwkFBQYDCQAAAAAAAQIDBBEFBiEHEhMxQVFhcYEiUpGSsTIzYnKhFCNCc4LBJFOyFSVDY6Kz0dLw/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/ALbAAAAAAAAAAAEkAACQIJAAAAAAAAAAAAAAAAAAAAAAAIAAAAAAABJAAAAkCCSDSt5+UvCwnKqrXKyI8HCp6VRl3Ss6vRagbsChdqcqO1L2+jnVjQ7FRDWSXjKeur+HkYC/ejaNj1ltDN1/BfZX+kGgOmQcz4+9W0q3rDaGZr+O6dv+ts2HZfKntOlpWunJh2q2HMm/KcNNH6PyAvcGnbr8ouDnyjVJvHyX1VXfZk+6FnU34PR+BuIAAAAAAAAAAAAAAAAEAAAAAAJIAAkAD8W2RhGU5yUYRi5OUnolFLVts/ZT3K/va7LHs3Hn+7g/38ot+1Zwaq8l2+Oi7GB4N/8AlDszZSxsKUq8LjGVi1jZd/6w8O3t7ivwAAAAAAAWRyf8o88eUMXaE3PG4RhkS1lZW9dEpv8Aih49a8uqtwB1fFppNNNNJprimu8kqnke3tcv925Em2oylROTbei4yo1fdxa8E12ItYAAAAAAAAAAAAAAgAAAAABIAAADFb0bWWDgZGU9G66pOKfba+EI+smjme22U5ynNuU5SlOUn1uUnq38WXPy3Zrhs+ilP73JTa/DXFy+riUqAAAAAAAAAAAH2xMqdFsLqpONtc42RkuyUXqvTwOntj7Qhl4tOTD7FtULEl2arivR6nLZe3IzluzZHRt6unJvgvyS0sX6zkBvQAAAAAAAAAAAACAAABIAAAAAAKm5eG9dnr+HTOfr+50+rKoLl5csRyw8W5L7vInBvujZD/zCJTQAAAAAAAAAAAC5uQt/4LM7v2uv/tIpkvLkUxeZsqdjX3uZa14xhGMNfmUvgBvwAAAAAAAAAAAkgCCQAAAAAAAAAMDvxsh52zMmiK1tdfPr/mw9qK9dNPU5ua04NNPufBp9x1eUXyrbqvDy3lUwf7JkTlJ81ezXe+MovuUuLXqu4DQwAAAAAAAAAB+oQcmoxTlKTUVFcW5N6KKXe2dO7t7KWDg4+KtNaqYxlp22v2pv1k2VTyPbqu+//aN0f8PTKSqTX28lfxL8MOP9WncXOAAAAAAAAAJAAEEkAAAAAAAAAAYLefezD2ZBPIs1sabjRXpK6S79OxeL0RV+2eVrOtcli11Y0OOkmlfdp2PWXsp+jAu08u09n1ZVE6L4KdM4uMov6p9jXY+w55s332tLr2jk/wBLhX+kUjdtz+VZrm07TWq4JZcIpPztgvrH4Aahvtubfsu3XSVmJKT5l6i9EteELOyMuPr2dqWsHUydGZRwdORjWw7ObbVODXwaK33n5JIzbs2bbGvrf7Ne5OGv4LOuPk9fNAVCDM7W3V2hiNq/CyFH/MhXK6nz58NUvXQw81zftax/N7P1AgE1py+ynJ/hTl9DObI3P2lmNdDhZHNf/FuhKinTv589E/TUDBG4bhbjXbUsVlinVgRl7VrTTsa666u/xl1LzN03X5JaamrdpWRyJ9ax6udGhP8AHPrn5LReZYeVlUYtPPtnVRj1xS1k411xiupJf2QH0xMaumuFVMI10wioRrgtIxiuxH1Ke3v5VbLHKnZidUOp5M4p2S/JF8IrxfHwRqNW/G1ofZ2jk/1OFv8AriwOjwUtsblczK2ll01ZEO2Va6C3Tv8Adb+BaO7e82JtKtzxbdZR051U/Yuh+aPd4rgBmAAAJBAEgEASQSQAAAAAADVd/wDe6OysZc3SWXapKqD6lp12SXurX1fA2bIvjVCVlklGuEZTlJ9Silq2c0707cs2jm25M2+a5ONcX/BQm+ZH4cX4tgeDNy7b7ZW3WSstm9ZTm9ZN/wD3YfAAAAAM1u5vRmbNnrjWtVt6ypn7dM/OPY/FaMtLYPKzh3aRy654tnD21+9ob81xj6r1KTAHUuztq4+THn42RTbHvqsjPTzSeq9T0Tx4S4yrrb75Qi3+qOU4NxkpRbjJdUovmyXk0ZKneHOgtIZuWl/Osf1YHTUKYR4xhCL74wjF/ojy7T2zi4y52Tk0VL/m2RjJ+S11foc3X7fzbFpPMy2v51i+jMc+Lcnxk+uT4t+b7QLo2/yt4tWsMKqeRP8AzJ/uqE/X2pfBLxKs3g3iy9o2c/Kuc0nrGuK5lMOGnsxX1er8TEgAAAB6dm7Quxbo349kq7o9U4/qmu1PuZ5gB0fuPvVXtXF6RLmXwahbV7s9NedHvi+x+a7DYjm3cjeGWzM+u/V9A30d0VxTpfW9O+L4ry8TpCMk0nFpxaTTXFNPqaA/QAAAAAQSAIAAAAAaDyy7W6DZqx4vSeTYoPTr6GDUp+j4L1KNN+5aM/pdqQq19mjGhHT8c25Sfw5vwNBAAAAAAAAAAAAAAAAAAAAAABf3JNtn9r2VCMm+lxpvHlr2xUVKuXyyS84soEsrkP2jzM3Ixm+FuOrUuzn1SSfrpP8AQC5wAAAAAAgAAAABE581OT6km/RcQOad88zp9q5tnWv2u6C/LXLo1+kTDH0vnzpzk+uU5y9XJs+YAAAAAAAAAAAAAAAAAAAAAANm5NcrottYUtdFK2db8rK5R/uaye7Yd3R5uLP3cvGk/JWx1/TUDqMEsgAAAAAAgAAD4Z70otfdTa/+hn3PPtGOuPcl1um1fGDA5XTAitFoAAAAAAAAAAAAAAAAAAAAAAAfumfNnGXdOL+D1PwSqnNqC65NRXnJ6L6gdYvrIJl1kAAAAAAEAAAGteD6v7AAcwbxbOeJnZOPJadHkWxWvbXztYP1i0Y4uPlf3TnfFbQx4uVldfNuhH7Uqk+FiXa46vXw8inEAAAAAAAAAAAAAAAAAAAAAADO7jbN/a9q4dOjcenjZLTshX+8k34ez+pgm9OL6i8eSXdKeDRLLyY83KvhFQg17VeNwftd0pPR6dyQFgtkAAAAAAAEAAAAABXO+HJbVlTlfgShj3PVumS0x5S71pxg36rwLGAHNe090NpYuvTYV6im1z649PB+KcNeBhLk6/vE4P8AGnD6nWA1A5L6eHvw+ZDp4e/D5kdaajUDkvp4e/D5kOnh78PmR1pqNQOS+nh78PmQ6eHvw+ZHWmo1A5L6eHvw+ZDp4e/D5kdaDUDkvp4e/D5kOnh78PmR1pqNQOS+nh78PmQ6eHvw+ZHWmoA5Qpi7Pu1Kf5E5/Qz2ytytp5enQ4Vyi395clj1rxbnpw8tTpLUNgaBuZyZUYM1flyjlZK0cY83/DVy95J8Zy8Xw8Df2yAAAAAAAAABAAAAAAAAABIAAAAAAAAAAAAAAAAAAAAAAAAAAAASQBAAAAAAAAJBAAkEACQAAAAAAAAAAAAAAAAAAAAAkgASAAP/2Q==';
        if (old_user) result.current_park_id = old_user.current_park_id;
        await AsyncStorage.setItem('user', JSON.stringify(result));
        return result;
    })
    .catch(e => {
        console.log(e);
    });
    return result;
}

const getUser = async () => {
    var user_json = await AsyncStorage.getItem('user');
    return JSON.parse(user_json);
}

const removeUser = async () => {
    await AsyncStorage.removeItem('user');
}

const getParks = async () => {
    var user_json = await AsyncStorage.getItem('user');
    var user = JSON.parse(user_json);
    var parks = user.accounts;
    return parks;
}

const selectPark = async (id) => {
    var parks = await getParks();
    var user = await getUser();
    user.current_park_id = id;
    user.base_balance = parks.find(park => park.id == id).balance;
    await AsyncStorage.removeItem('user');
    await AsyncStorage.setItem('user', JSON.stringify(user));
}

const getMedias = async (park_id) => {
    var token = await AsyncStorage.getItem('token');
    token = JSON.parse(token);

    var headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token.access_token}`
    };
    var method = 'GET';
    var result = await fetch(`${common.url}/api/media-sliders?park_id=${park_id}`, {
        headers,
        method,
    })
    .then(async data => {
        let result = await data.json();
        return result;
    })
    .catch(e => {
        console.log(e);
    });
    return result;
}

const getAddress = async () => {
    var token = await AsyncStorage.getItem('token');
    token = JSON.parse(token);

    var headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token.access_token}`
    };
    var method = 'GET';
    var result = await fetch(`${common.url}/api/park-addresses`, {
        headers,
        method,
    })
    .then(async data => {
        let result = await data.json();
        return result;
    })
    .catch(e => {
        console.log(e);
    });
    return result;
}

const getSupport = async () => {
    var token = await AsyncStorage.getItem('token');
    token = JSON.parse(token);

    var headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token.access_token}`
    };
    var method = 'GET';
    var result = await fetch(`${common.url}/api/park-addresses`, {
        headers,
        method,
    })
    .then(async data => {
        let result = await data.json();
        return result;
    })
    .catch(e => {
        console.log(e);
    });
    return result;
}

const getBaigaResult = async (week, group_id, url = '') => {
    var token = await AsyncStorage.getItem('token');
    token = JSON.parse(token);

    var headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token.access_token}`
    };
    var method = 'GET';
    var request_url = week == 'last' ? `api/ranking/top?group_id=${group_id}` : `api/ranking/history?week=${week}&group_id=${group_id}`;

    var result = await fetch(url.length == 0 ? (`${common.url}/${request_url}`) : url, {
        headers,
        method,
    })
    .then(async data => {
        let result = await data.json();
        return result;
    })
    .catch(e => {
        console.log(e);
    });
    return result;
}

const getBaigaSelf = async () => {
    var token = await AsyncStorage.getItem('token');
    token = JSON.parse(token);

    var headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token.access_token}`
    };
    var method = 'GET';
    var request_url = 'api/ranking/self';

    var result = await fetch(`${common.url}/${request_url}`, {
        headers,
        method,
    })
    .then(async data => {
        let result = await data.json();
        return result;
    })
    .catch(e => {
        console.log(e);
    });
    return result;
}

const getBaigaWeeks = async () => {
    var token = await AsyncStorage.getItem('token');
    token = JSON.parse(token);

    var headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token.access_token}`
    };
    var method = 'GET';
    var request_url = 'api/ranking/weeks';

    var result = await fetch(`${common.url}/${request_url}`, {
        headers,
        method,
    })
    .then(async data => {
        let result = await data.json();
        return result;
    })
    .catch(e => {
        console.log(e);
    });
    return result;
}

const getVersion = async (os) => {
    var token = await AsyncStorage.getItem('token');
    token = JSON.parse(token);

    var headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token.access_token}`
    };
    var method = 'GET';
    var request_url = 'api/app/latest-version';

    var result = await fetch(`${common.url}/${request_url}?os=${os}`, {
        headers,
        method,
    })
    .then(async data => {
        let result = await data.json();
        return result;
    })
    .catch(e => {
        console.log(e);
    });
    if (os == 'ios') return '2.52';
    return result;
}


const profile = {
    getVersion,
    userExists,
    setUser,
    getUser,
    removeUser,
    getParks,
    selectPark,
    getMedias,
    getAddress,
    getSupport,
    getBaigaResult,
    getBaigaSelf,
    getBaigaWeeks,
    getGroups
};

export default profile;