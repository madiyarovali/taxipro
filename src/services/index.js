import user from './user.service';
import profile from './profile.service';
import cashout from './cashout.service';
import payment from './payment.service';
import common from './common.service';
import competition from './competition.service';

export default {
    user,
    profile,
    cashout,
    payment,
    common,
    competition
};