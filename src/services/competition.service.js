import AsyncStorage from '@react-native-community/async-storage';
import common from './common.service';

const get = async (url) => {
    var token = await AsyncStorage.getItem('token');
    token = JSON.parse(token);

    var headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token.access_token}`
    };
    var method = 'GET';

    var result = await fetch(`${common.url}${url}`, {
        headers,
        method,
    })
    .then(async data => {
        let result = await data.json();
        return result;
    })
    .catch(e => {
        throw e;
    });
    
    return result;
}

const post = async (url, body) => {
    var token = await AsyncStorage.getItem('token');
    token = JSON.parse(token);

    var headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token.access_token}`
    };
    var method = 'POST';

    var result = await fetch(`${common.url}${url}`, {
        headers,
        method,
        body
    })
    .then(async data => {
        let result = await data.json();
        return result;
    })
    .catch(e => {
        throw e;
    });
    
    return result;
}


const competition = {
    get,
    post
};

export default competition;