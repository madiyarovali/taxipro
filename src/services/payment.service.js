import AsyncStorage from '@react-native-community/async-storage';
import common from './common.service';

const getHistory = async (page = 1) => {
    var token = await AsyncStorage.getItem('token');
    token = JSON.parse(token);

    var headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token.access_token}`
    };
    
    var method = 'GET';
    var result = await fetch(`${common.url}/api/payments?page=${page}`, {
        headers,
        method,
    })
    .then(async data => {
        let result = await data.json();
        return result;
    })
    .catch(e => {
        console.log(e);
    });

    return result;
}

const getHistoryPagesCount = async () => {
    var history = await getHistory();
    return history.total;
}

const proceed = async ({ type, card_id }, amount, operation, account_id, current_version) => {
    var token = await AsyncStorage.getItem('token');
    token = JSON.parse(token);

    var headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token.access_token}`
    };
    var body = JSON.stringify({
        card_id, amount, account_id, type, current_version
    });
    var method = 'POST';
    var result = await fetch(`${common.url}/api/${operation}`, {
        headers,
        body,
        method,
    })
    .then(async data => {
        let result = await data.json();
        return [result, data];
    })
    .catch(e => {
        console.log(e);
    });

    return result;
}

const payment = {
    getHistory,
    getHistoryPagesCount,
    proceed
}

export default payment;