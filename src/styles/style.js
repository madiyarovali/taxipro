import { StyleSheet, Platform } from 'react-native';

const font_family = Platform.OS == 'ios' ? 'System' : 'HelveticaBold';
module.exports = StyleSheet.create({
    title: {
      fontSize: 36,
      fontFamily: font_family,
      fontWeight: 'bold',
      color: '#21201F'
    },
    subtitle: {
      fontSize: 16,
      marginTop: 28
    },
    scrollView: {

      backgroundColor: 'white',
      fontFamily: "HelveticaRegular",
      width: '100%',
    },
    view: {
      backgroundColor: 'white',
      fontFamily: "HelveticaRegular",
      width: '100%',
      height: '100%'
    },
    topDistance: {
      paddingTop: 0
    },
    center: {
      justifyContent: 'center'
    },

    label: {
      color: '#A7A4A1',
      fontSize: 14,
      position: 'absolute',
      top: -10,
    },
    primaryBtn: {
      marginLeft: 8,
      marginRight: 8
    },
    secondaryBtn: {
      backgroundColor: '#EDECE8'
    },
    exitBtn: {
      alignSelf: 'flex-end',
      marginRight: 8,
    },
    backBtn: {
      alignSelf: 'flex-start',
      marginLeft: -18,
      marginTop: Platform.OS == 'ios' ? 30 : 10,
      width: 70
    },
    backBtnWrapper: {
      flex: 1,
    },
    actionsBtnWrapper: {
      //paddingTop: Platform.OS == 'ios' ? 45 : 25,
      paddingHorizontal: 16,
      paddingVertical: 16,
      backgroundColor: '#ffffff',
      flexDirection: 'row'
    },
    avatarWrapper: {
      justifyContent: 'center',
      alignItems: 'center',
      marginBottom: 16
    },
    avatar: {
      width: 90,
      height: 90
    },
    username: {
      fontSize: 20
    },
    username1: {
      fontSize: 26,
      fontWeight: '700'
    },
    grayBtnText: {
      color: '#A7A4A1',
      fontSize: 12
    },
    profileMain: {
      flexDirection: 'row'
    },
    profileMainInfo: {
      flex: 1
    },
    moneyInput: {
      borderWidth: 1,
      borderRadius: 5,
      textAlign: 'center',
      fontSize: 36,
      marginTop: 20,
      paddingVertical: 4,
      fontWeight: 'bold',
      fontFamily: font_family,
      width: '100%'
    },
    cashoutWrapper: {
      marginTop: 20,
      alignItems: 'center'
    },
    paymentWrapper: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      paddingHorizontal: 16
    }, 
    cashoutLabel: {
      fontSize: 14
    }
});