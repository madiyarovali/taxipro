// import React from 'react';
// import {TouchableOpacity, View, Text, StyleSheet, Image} from 'react-native';
// import BackIcon from '../../assets/icons/back';

// const TopComponent = ({navigation}) => (
//   <TouchableOpacity onPress={() => navigation.goBack()}>
//     <View
//       style={{
//         flexDirection: 'row',
//         padding: 16,
//         // borderBottomWidth: 1,
//         // borderColor: '#cecece',
//         //marginBottom: 16,
//       }}>
//       <Image
//         style={styles.image}
//         source={require('src/assets/images/back.png')}
//       />
//       <Text style={{marginLeft: 10}}>Назад</Text>
//     </View>
//   </TouchableOpacity>
// );

// const styles = new StyleSheet.create({
//   image: {
//       width: 20,
//       height: 20,
//       marginRight: 10,
//       resizeMode: 'contain',
//   },
// });

// export default TopComponent;

import React from 'react';
import NavBar from 'src/components/NavBar';
export default class TopComponent extends React.Component {
  constructor (props) {
    super(props);
  }
  render () {
    return (
      <NavBar navigation={this.props.navigation}/>
    );
  }
}