import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

const PrimaryButton = props => {
    return (
        <TouchableOpacity onPress={props.onPress}  disabled={props.loading || false}>
            <View style={{ ...styles.button, ...props.style }}>
                <Text style={{ ...styles.buttonText, ...styles.textStyling}}>{props.children}</Text>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    button: {
        backgroundColor: "#FCE000",
        paddingVertical: 20,
        alignItems: 'center',
        marginLeft: 8,
        marginRight: 8,
        marginBottom: 10,
        borderRadius: 5
    },
    buttonText: {
        color: "#212121",
        fontSize: 18,
        textAlign: 'center'
    }
})

export default PrimaryButton;