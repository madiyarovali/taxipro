import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';

const PaymentButton = props => {
    return (
        <TouchableOpacity onPress={props.onPress}>
            <View style={{ ...styles.button, ...styles.style}}>
                <Image 
                    style={styles.icon}
                    source={props.icon}
                />
                <Text style={{ ...styles.buttonText, ...styles.textStyling}}>{props.children}</Text>
                {
                    // props.type == 'carwash'
                    // ? <Text style={styles.addressLabel}>ул. Байзакова 78А</Text>
                    // : <Text style={styles.addressLabel}>ул. Байзакова 78А</Text>
                }
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    button: {
        backgroundColor: "white",
        borderRadius: 10,
        paddingVertical: 20,
        alignItems: 'center',
        marginLeft: 8,
        marginRight: 8,
        marginBottom: 10,
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        width: 140,
        elevation: 7,
    },
    icon: {
        width: '100%',
        height: 60,
        aspectRatio: 1,
    },
    buttonText: {
        color: "#000",
        fontSize: 14,
        marginTop: 8,
        textAlign: 'center'
    },
    addressLabel: {
        color: 'gray',
        fontSize: 10
    }
})

export default PaymentButton;