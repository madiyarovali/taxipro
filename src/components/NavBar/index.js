import React from 'react';
import {
    View,
    Text,
    StyleSheet
} from 'react-native';
import BackButton from './BackButton'
import ExitButton from './ExitButton'

export default class NavbarComponent extends React.Component {
    constructor (props) {
        super(props);
    }

    render () {
        return (
            <View
                style={styles.wrapper}
            >
                {
                    ['Home', 'Verify', 'PinLock', 'Park'].includes(this.props.navigation.state.routeName)
                    ? <ExitButton navigation={this.props.navigation} showText={false}/>
                    : <BackButton navigation={this.props.navigation} showText={true}/>
                }
                <Text style={styles.text}>
                    Такси.Про
                </Text>
            </View>
        );
    }
}

const styles = new StyleSheet.create({
    wrapper: {
        position: 'relative',
        marginVertical: 20,
        paddingHorizontal: 10,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    text: {
        marginVertical: 7,
        textAlign: 'center',
        fontSize: 20,
        fontWeight: '700'
    }
});