import React from 'react';
import {
    TouchableOpacity,
    Image,
    Text,
    StyleSheet
} from 'react-native';

export default class BackButtonComponent extends React.Component {
    constructor (props) {
        super(props);
    }

    goBack () {
        if (this.props.navigation) this.props.navigation.goBack();
    }

    render () {
        return (
            <TouchableOpacity
                onPress={() => this.goBack()}
                style={styles.button}
            >
                <Image
                    style={styles.image}
                    source={require('src/assets/images/back.png')}
                />
                {
                    this.props.showText
                    ? <Text>Назад</Text>
                    : null
                }
            </TouchableOpacity>
        );
    }
}

const styles = new StyleSheet.create({
    image: {
        width: 20,
        height: 20,
        marginRight: 10,
        resizeMode: 'contain',
    },
    button: {
        position: 'absolute',
        top: 0,
        left: 15,
        display: 'flex',
        flexDirection: 'row',
        alignSelf: 'flex-start',
        padding: 10,
        borderRadius: 100,
    }
});