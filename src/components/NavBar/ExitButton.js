import React from 'react';
import {
    TouchableOpacity,
    Text,
    StyleSheet
} from 'react-native';
import services from 'src/services';
import LogoutIcon from 'src/assets/icons/logout';

export default class BackButtonComponent extends React.Component {
    constructor (props) {
        super(props);
    }

    exit () {
        services.user.removeUser();
        services.profile.removeUser();
        this.props.navigation.navigate('Login');
    }

    render () {
        return (
            <TouchableOpacity
                onPress={() => this.exit()}
                style={styles.button}
            >
                <LogoutIcon/>
                {
                    this.props.showText
                    ? <Text>Выйти</Text>
                    : null
                }
            </TouchableOpacity>
        );
    }
}

const styles = new StyleSheet.create({
    image: {
        width: 20,
        height: 20,
        marginRight: 10,
        resizeMode: 'contain',
    },
    button: {
        position: 'absolute',
        top: 0,
        left: 15,
        display: 'flex',
        flexDirection: 'row',
        alignSelf: 'flex-start',
        padding: 10,
        borderRadius: 100,
    }
});