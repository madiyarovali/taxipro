import React from 'react'
import { View, StyleSheet, ActivityIndicator, Dimensions } from 'react-native'

var width = Dimensions.get('window').width
var height = Dimensions.get('window').height

export default class LoginScreen extends React.Component {
    render() {
        return (
            <View style={styles.loading_wrapper}>
                <View style={styles.loading}>
                    <ActivityIndicator size="large" color="#fce100" />
                </View>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    loading: {
        backgroundColor: 'white',
        width: 100,
        height: 100,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10
    },
    loading_wrapper: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        width: width,
        height: height + 50,
        zIndex: 10,
        backgroundColor: '#0000007a'
    }
});
