import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { TextInputMask } from 'react-native-masked-text'

var globalStyles = require('../../styles/style')

export default class MoneyInput extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            borderColor: '#E2E2E2'
        };
    }
    onFocus() {
        this.setState({
            borderColor: '#FCE000'
        })
    }

    onBlur() {
        this.setState({
            borderColor: '#E2E2E2'
        })
    }

    render() {
        return (
            <TextInputMask
                returnKeyType='done'
                type={'money'}
                options={{
                    precision: 0,
                    separator: ' ',
                    delimiter: ' ',
                    unit: '',
                    suffixUnit: ''
                }}
                onBlur={ () => this.onBlur() }
                onFocus={ () => this.onFocus() }
                style={{ ...globalStyles.moneyInput, ...{ borderColor: this.state.borderColor }}}
                value={this.props.value}
                keyboardType='number-pad'
                onChangeText={this.props.onChangeText}
            />
            )
    }
}