import React from 'react'
import { View, StyleSheet, 
    TouchableOpacity,
    Text,
    Image,
    Platform
} from 'react-native'



export default class BottomNav extends React.Component {

    profile() {
        this.props.navigation.navigate('Profile');
    }
    home() {
        this.props.navigation.navigate('Home');
    }
    baiga() {
        this.props.navigation.navigate('Baiga');
    }
    render() {
        let { page } = this.props;
        return (
            <View
                style={{
                    display: 'flex',
                    position: 'absolute',
                    bottom: 0,
                    paddingBottom: Platform.OS == 'ios' ? 20 : 0,
                    alignSelf: 'flex-end',
                    flexDirection: 'row',
                    borderTopColor: '#cecece',
                    borderTopWidth: 1,
                    backgroundColor: "#FFFFFF"
                }}>
                <TouchableOpacity
                    onPress={() => {
                        this.home();
                    }}
                    style={styles.button}
                    >
                    <Image
                    source={page == 'home' ? require('./../../assets/images/home-colored.png') : require('./../../assets/images/home.png')}
                        style={styles.image}
                    />
                    <Text
                    style={{
                        color: page == 'home' ? '#E2574C' : '#BEC2C4'
                    }}
                    >Главная</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => {
                        this.profile();
                    }}
                    style={styles.button}>
                    <Image
                    source={page == 'profile' ? require('./../../assets/images/user-colored.png') : require('./../../assets/images/userasd.png')}
                    style={styles.image}
                    />
                    <Text
                    style={{
                        color: page == 'profile' ? '#E2574C' : '#BEC2C4'
                    }}>Профиль</Text>
                </TouchableOpacity>
                {
                    this.props.show_baiga
                    ? 
                    <TouchableOpacity
                        onPress={() => {
                            this.baiga();
                        }}
                        style={styles.button}>
                        <Image
                        source={page == 'baiga' ? require('./../../assets/images/baiga-colored.png') : require('./../../assets/images/baiga.png')}
                        style={styles.image}
                        />
                        <Text
                        style={{
                            color: page == 'baiga' ? '#E2574C' : '#BEC2C4'
                        }}>Байга</Text>
                    </TouchableOpacity>
                    :
                    <></>
                }
            </View>
            
        );
    }
}


const styles = StyleSheet.create({
    button: {
        flex: 1, 
        alignItems: 'center', 
        paddingVertical: 15
    },
    image: {
        width: 20, 
        height: 20, 
        marginBottom: 5
    }
});
