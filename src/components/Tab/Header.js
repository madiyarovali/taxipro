// props: [activeIndex, titles]

import React from 'react';
import {
    View,
    TouchableOpacity,
    Text,
    StyleSheet
} from 'react-native';

export default class TabHeaderComponent extends React.Component {
    constructor (props) {
        super(props);
    }

    setActiveTabStyle (index) {
        if (index == this.props.activeIndex) {
            return {
                backgroundColor: '#FCE000'
            };
        }
    }

    switchTo (index) {
        this.props.switchTabs(index);
    }

    render () {
        return (
            <View style={styles.wrapper}>
                <View style={styles.tabs}>
                    {
                        this.props.titles.map((title, index) => {
                            return (
                                <TouchableOpacity
                                    onPress={() => this.switchTo(index)}
                                    key={index}
                                    style={[styles.tab_button, this.setActiveTabStyle(index)]}
                                >
                                    <Text style={styles.tab_title}>
                                        {title}
                                    </Text>
                                </TouchableOpacity>
                            )
                        })
                    }
                </View>
            </View>
        );
    }
}

const styles = new StyleSheet.create({
    wrapper: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 20,
        paddingHorizontal: 10,
    },
    tabs: {
        flexDirection: 'row',
        alignSelf: 'flex-start',
        borderWidth: 1,
        borderColor: '#FCE000',
        paddingHorizontal: 3,
        paddingVertical: 3,
        borderRadius: 10
    },
    tab_button: {
        paddingVertical: 5,
        paddingHorizontal: 15,
        minWidth: 150,
        borderRadius: 7
    },
    tab_title: {
        fontSize: 20,
        textAlign: 'center'
    }
});