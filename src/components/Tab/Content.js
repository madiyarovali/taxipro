// props: [activeIndex, contents]

import React from 'react';
import {
    View,
    Text
} from 'react-native';

export default class TabContentComponent extends React.Component {
    constructor (props) {
        super(props);
    }

    render () {
        return (
            <View>
                {
                    this.props.contents[this.props.activeIndex]
                }
            </View>
        );
    }
}