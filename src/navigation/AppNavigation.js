import { createStackNavigator } from 'react-navigation-stack'
import HomeScreen from './../screens/home/index'
import StaticScreen from './../screens/home/static'
import ProfileScreen from './../screens/home/profile'
import ParkScreen from './../screens/home/park'
import CashoutScreen from './../screens/cashout/index'
import HistoryScreen from './../screens/cashout/history'
import CardFormScreen from './../screens/cashout/form'
import PaymentScreen from './../screens/payment/index'
import PaymentFormScreen from './../screens/payment/form'
import SuccessScreen from './../screens/common/success'
import ProcessingScreen from './../screens/common/processing'
import BaigaScreen from 'src/screens/baiga'
import BaigaInfoScreen from 'src/screens/baiga/Info'
import CompetitionScreen from 'src/screens/competition'
import CompetitionInfoScreen from 'src/screens/competition/Info'

const AppNavigation = createStackNavigator(
    {
        Home: { screen: HomeScreen },
        Static: { screen: StaticScreen },
        Profile: { screen: ProfileScreen },
        Park: { screen: ParkScreen },
        
        Cashout: { screen: CashoutScreen },
        History: { screen: HistoryScreen },
        CardForm: { screen: CardFormScreen },
        
        Payment: { screen: PaymentScreen },
        PaymentForm: { screen: PaymentFormScreen },
        
        Processing: { screen: ProcessingScreen, gestureEnabled: false },
        Success: { screen: SuccessScreen },

        Baiga: { screen: BaigaScreen },
        BaigaInfo: { screen: BaigaInfoScreen },

        Competition: { screen: CompetitionScreen },
        CompetitionInfo: { screen: CompetitionInfoScreen },
    },
    {
        initialRouteName: 'Park',
        headerMode: 'none'
    }
)

export default AppNavigation