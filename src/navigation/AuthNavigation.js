import { createStackNavigator } from 'react-navigation-stack'
import LoginScreen from './../screens/login/index'
import VerifyScreen from './../screens/login/verify'
import PinLockScreen from './../screens/login/lockscreen'

const AuthNavigation = createStackNavigator(
    {
        Login: { screen: LoginScreen },
        Verify: { screen: VerifyScreen },
        PinLock: { screen: PinLockScreen }
    },
    {
        initialRouteName: 'Login',
        headerMode: 'none'
    }
)

export default AuthNavigation