import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  VirtualizedList,
  TouchableOpacity,
  Platform,
  ActionSheetIOS,
  SafeAreaView,
} from 'react-native';
// import {Picker} from '@react-native-picker/picker';
import services from '../../services';
import {Picker} from '@react-native-picker/picker'; 
import BottomNav from '../../components/navigations/bottom';
import Icon from 'react-native-vector-icons/Ionicons'
import NavBar from 'src/components/NavBar';
var globalStyles = require('../../styles/style');

const font_family = Platform.OS == 'ios' ? 'System' : 'HelveticaBold1';
export default class BaigaScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
      loading: true,
      list: [],
      week: 'last',
      ios_week: 'Текущая неделя',
      next_url: '',
      self: {
        current_rank: {
          score: 0,
          rank: 0,
        },
      },
      weeks: [],
      groups: [],
      group_index: 0
    };
  }

  baigaInfo() {
    this.props.navigation.navigate('BaigaInfo');
  }

  async componentDidMount() {
    this.init();
  }

  async init() {
    var user_exists = await services.profile.userExists();
    if (!user_exists) {
      await services.profile.setUser();
    }

    let week = this.state.week;
    var user = await services.profile.getUser();
    var groups = await services.profile.getGroups();
    var group_id = groups[this.state.group_index].id;
    var baiga = await services.profile.getBaigaResult(week, group_id);
    var next_url;
    var list;
    next_url = baiga.next_page_url;
    list = baiga.data;
    if (typeof next_url == 'undefined') {
      next_url = baiga.links.next;
    }

    var weeks = await services.profile.getBaigaWeeks();
    weeks.unshift({});
    for (let i = 0; i < list.length; i++) {
      if (list[i].driver_name) {
        list[i].name = list[i].driver_name;
        list[i].reward = list[i].reward_title;
      }
    }
    var baigaSelf = await services.profile.getBaigaSelf();
    var self = {...this.state.self};
    if (baigaSelf.rank) self.current_rank.rank = baigaSelf.rank;
    if (baigaSelf.score) self.current_rank.score = baigaSelf.score;
    this.setState({self});
    var loading = false;
    this.setState({user, loading, list, self, weeks, next_url, groups});
  }

  async onValueChange(itemValue, itemIndex) {
    this.setState({week: itemValue, loading: true});
    let group_id = this.state.groups[this.state.group_index].id;
    let baiga = await services.profile.getBaigaResult(itemValue, group_id);
    var list = baiga.data;
    var next_url = baiga.next_page_url;
    if (typeof next_url == 'undefined') {
      next_url = baiga.links.next;
    }
    
    for (let i = 0; i < list.length; i++) {
      if (list[i].driver_name) {
        list[i].name = list[i].driver_name;
        list[i].reward = list[i].reward_title;
      }
    }
    this.setState({list, next_url, loading: false});
  }

  removeUser() {
    services.user.removeUser();
    services.profile.removeUser();
    this.props.navigation.navigate('Login');
  }

  showIosDropdown() {
    let options = [];
    let values = [];
    this.state.weeks.map(function (item, index) {
      let option =
        index > 0 ? `${item.start_date} - ${item.end_date}` : 'Текущая неделя';
      let value = index > 0 ? item.week : 'last';
      options.push(option);
      values.push(value);
    });
    return ActionSheetIOS.showActionSheetWithOptions(
      {
        options: options,
      },
      (buttonIndex) => {
        this.onValueChange(values[buttonIndex]);
        this.setState({
          week: values[buttonIndex],
          ios_week: options[buttonIndex],
        });
      },
    );
  }

  activeBadgeStyle(index) {
    if (index == this.state.group_index) {
      return {
        backgroundColor: '#27AE60',
      }
    }
  }

  activeBadgeTextStyle(index) {
    if (index == this.state.group_index) {
      return {
        color: 'white',
      }
    }
    return {
      color: '#27AE60',
    }
  }

  changeGroup (index) {
    this.setState({ group_index: index });
    this.refresh();
  }

  renderListHeader = () => {
    return (
      <View>
        <NavBar navigation={this.props.navigation}/>
        <View style={{ ...styles.parkWrapper, ...{height: 60} }}>
          <View style={styles.titleWrapper}>
            <View style={{
              flexDirection: 'row',
              justifyContent: 'center',
              marginTop: 20
            }}>
                  <Text style={styles.title}>Байга</Text>
                  <TouchableOpacity
                      onPress={() => { this.baigaInfo() }}
                      style={{
                          marginTop: 3
                      }}
                  >
                    <View style={styles.info}>
                        <Icon 
                            name={"information-circle-outline"}
                            size={20}
                            color="#BEC2C4"
                        />
                        <Text style={{ color: '#BEC2C4' }}>что это?</Text>
                    </View>
                </TouchableOpacity>

              </View>
            <View>
              {Platform.OS == 'ios' ? (
                <TouchableOpacity
                  onPress={() => this.showIosDropdown()}
                  style={{ ...styles.select, ...styles.iosSelect}}>
                  <Icon 
                    name={"calendar-outline"}
                    size={20}
                    style={{
                      marginRight: 5
                    }}
                  />
                  <Text style={{
                    textAlign: 'right',
                    marginRight: 15
                  }}>{this.state.ios_week}</Text>
                </TouchableOpacity>
              ) : (
                <View style={{ display: 'flex', flexDirection: 'row' }}>
                  <Icon 
                    name={"calendar-outline"}
                    size={20}
                    style={{
                      marginTop: 30
                    }}
                  />
                  <Picker
                      selectedValue={this.state.week}
                      style={styles.select}
                      onValueChange={(itemValue, itemIndex) => this.onValueChange(itemValue, itemIndex)}
                  >
                      {
                          this.state.weeks.map(function (item, index) {
                              let option = index > 0 ? `${item.start_date} - ${item.end_date}` : 'Текущая неделя';
                              let value = index > 0 ? item.week : 'last';
                              return (
                                  <Picker.Item
                                      label={option}
                                      value={value}
                                      key={index} 
                                      mode="dropdown"
                                  />
                              )
                          })
                      }
                  </Picker>
                </View>
              )}
            </View>
          </View>
        </View>
        <View style={styles.badges_wrapper}>
          {
            this.state.groups.map((group, index) => {
              return (
                <TouchableOpacity
                  key={index}
                  style={[styles.badge, this.activeBadgeStyle(index)]}
                  onPress={() => this.changeGroup(index)}
                >
                  <Text style={this.activeBadgeTextStyle(index)}>
                    {group.title}
                  </Text>
                </TouchableOpacity>
              );
            })
          }
        </View>
        <View style={styles.city_wrapper}>
          {
            this.state.groups.length > 0
            ? <Text style={{ color: 'black', marginHorizontal: 5 }}>
            {Object.values(this.state.groups[this.state.group_index].cities).join(', ')} 
            </Text>
            : null
          }
        </View>
      </View>
    );
  };

  refresh = () => {
    this.init();
  };

  renderListItem = ({item, index}) => {
    return (
        <TouchableOpacity>
        <View style={styles.listItem}>
            <View style={styles.listIndex}>
                <Text style={[styles.boldText, styles.place]}>{index + 1 + ' место'}</Text>
                <Text style={styles.score}><Text style={styles.boldText}>{item.score}</Text> поездок</Text>
            </View>
            <View style={styles.parkInfoWrapper}>
                <Text style={styles.listItemValue}>{item.name}</Text>
                <Text style={styles.boldText}>{item.city} </Text>
            </View>
            <View style={styles.rewardInfo}>
              {
              item.reward
              ? <>
                  <Text style={[styles.reward, styles.boldText]}>{item.reward}</Text>
                  <Text style={styles.listItemLabel}>Приз</Text>
                </>
              : <>
              </>
            }
            </View>
            
        </View>
        </TouchableOpacity>
    );
  };

    getItem = (data, index) => {
        return data[index];
    }

    download = async () => {
        let week = this.state.week;
        let next_url = this.state.next_url;
        let group_id = this.state.groups[this.state.group_index].id;
        let baiga = await services.profile.getBaigaResult(week, group_id, next_url);
        let list = this.state.list;
        next_url = baiga.next_page_url;
        if (typeof next_url == 'undefined') {
          next_url = baiga.links.next;
        }
        for (let i = 0; i < baiga.data.length; i++) {
          if (baiga.data[i].driver_name) {
            baiga.data[i].name = baiga.data[i].driver_name;
            baiga.data[i].reward = baiga.data[i].reward_title;
          }
          list.push(baiga.data[i]);
        }
        this.setState({ list, next_url });
    }

  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#FFFFFF'}}>
        <View style={globalStyles.view}>
          {/* <TopComponent navigation={this.props.navigation}/> */}
          <View style={styles.mainBodyWrapper}>
            <View style={styles.container}>
              <VirtualizedList
                ref={(flat) => (this.flat = flat)}
                onRefresh={this.refresh}
                refreshing={this.state.loading}
                data={this.state.list}
                initialNumToRender={50}
                ListHeaderComponent={this.renderListHeader}
                renderItem={this.renderListItem}
                getItemCount={() => this.state.list.length}
                getItem={this.getItem}
                onEndReached={this.download}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>
            <TouchableOpacity
              style={{
                marginBottom: Platform.OS == 'android' ? 70 : 55,
              }}
              onPress={() => {
                if (this.state.self) {
                  let index = this.state.self.current_rank.rank;
                  if (index > 49) {
                    this.flat.scrollToEnd();
                  } else {
                    let params = {};
                    params.index = 0;
                    this.flat.scrollToIndex(params);
                  }
                }
              }}>
              <View style={[styles.listItem, styles.listItemOwn]}>
                <View style={styles.listIndex}>
                  <Text
                    style={[styles.index, styles.listItemOwnValue]}
                    numberOfLines={1}>
                    {this.state.self.current_rank.rank}
                  </Text>
                </View>
                <View style={styles.parkInfoWrapper}>
                  <Text style={[styles.listItemValue, styles.listItemOwnValue]}>
                    {this.state.user.full_name}
                  </Text>
                  <Text style={[styles.listItemLabel, styles.listItemOwnValue]}>
                    {`${this.state.self.current_rank.score} поездок`}
                  </Text>
                </View>
                <View style={styles.rewardInfo}>
                  <Text style={[styles.reward, styles.boldText]}>{this.state.self.current_rank.reward}</Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <BottomNav 
            page="baiga"
            show_baiga={1}
            navigation={this.props.navigation}
          />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
    city_wrapper: {
      backgroundColor: '#F4F4F4',
      borderRadius: 5,
      flexDirection: 'row',
      paddingHorizontal: 10,
      marginHorizontal: 10,
      marginBottom: 10,
      paddingVertical: 10
    },
    badge: {
        marginHorizontal: 10,
        paddingHorizontal: 13,
        paddingVertical: 6,
        borderRadius: 5
    },
    badges_wrapper: {
        marginVertical: 20,
        flexDirection: 'row'
    },
    info: {
        display: 'flex',
        flexDirection: 'row',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#BEC2C4',
        paddingHorizontal: 10,
        paddingVertical: 5,
        marginLeft: 10
    },
    score: {
        backgroundColor: 'rgba(39, 174, 96, 0.18)',
        borderRadius: 5,
        paddingHorizontal: 6,
        paddingVertical: 2,
        color: '#27AE60',
    },
    place: {
        color: '#27AE60',
        fontSize: 14
    },
    boldText: {
        fontWeight: '700'
    },
    reward: {
        fontSize: 12,
        color: '#FBB034',
        textAlign: 'right'
    },
    rewardInfo: {
        width: 100,
        textAlign: 'right',
        alignItems: 'flex-end'
    },
    titleWrapper: {
        justifyContent: 'space-between',
        flexDirection: 'row',
    },
    actionsBtnWrapper: {
        paddingHorizontal: 16,
        flexDirection: 'row',
    },
    iosSelect: {
        fontSize: 14,
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    citySelect: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginTop: 10

    },
    parkWrapper: {
        paddingLeft: 16,
        paddingRight: 16,
        //paddingTop: 5,
    },
    mainBodyWrapper: {
        flex: 1,
    },
    clearButton: {
        position: 'absolute',
        right: 16,
        bottom: 16,
    },
    title: {
        fontSize: 25,
        fontFamily: font_family,
        fontWeight: 'bold',
        marginBottom: 8,
        justifyContent: 'center',
    },
    subtitle: {
        fontSize: 20,
        fontWeight: '400',
        fontFamily: font_family,
        marginBottom: 40,
        textAlign: 'center',
        color: 'gray',
    },
    container: {
        flex: 1,
    },
    listItemLabel: {
        fontSize: 12,
        color: '#A7A4A1',
    },
    listItemValue: {
        fontSize: 15,
        textAlign: 'center'
    },
    listItemOwnValue: {
        color: 'white',
    },
    exitBtnWrapper: {
        flex: 1,
    },
    listItem: {
        paddingVertical: 8,
        paddingHorizontal: 16,
        borderBottomColor: '#E2E2E2',
        borderBottomWidth: 1,
        flexDirection: 'row',
    },
    select: {
        width: Platform.OS == 'ios' ? 190 : 190,
        height: 20,
        top: 30,
        textAlign: 'right',
        alignItems: 'flex-end',
        color: "#BEC2C4"
    },
    listItemOwn: {
        backgroundColor: '#27AE60',
        height: 60
    },
    listIndex: {
        width: 100,
        justifyContent: 'center',
        alignItems: 'flex-start',
        marginRight: 10,
    },
    index: {
        fontSize: 20,
        color: '#27AE60',
    },
    parkInfoWrapper: {
        alignSelf: 'center',
        justifyContent: 'center',
        flex: 1,
        alignItems: 'center',
    },
    arrowWrapper: {
        paddingRight: 8,
        alignItems: 'center',
        justifyContent: 'center',
    },
    item: {
        padding: 10,
        fontSize: 18,
        height: 44,
    },
    image_back: {
        width: 7,
        height: 15,
        resizeMode: 'stretch',
        marginTop: 10,
    },
});
