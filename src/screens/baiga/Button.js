import React from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image
} from 'react-native';

export default class CompetitionButtonComponent extends React.Component {
    constructor (props) {
        super(props);
    }

    baiga() {
        this.props.navigation.navigate('Baiga');
    }

    render () {
        if (this.props.park.park && this.props.park.park.is_baiga_enabled) {
            return (
                <TouchableOpacity
                    onPress={() => this.baiga()}
                    style={styles.button}
                >
                    <View style={styles.wrapper}>
                        <Image
                            source={require('src/assets/images/baiga-white.png')}
                            style={styles.image}
                        />
                        <Text style={styles.text}>
                            Байга
                        </Text>
                    </View>
                </TouchableOpacity>
            );
        }
        return null;
    }
}

const styles = StyleSheet.create({
    button: {
        backgroundColor: '#BB6BD9',
        flex: 1,
        borderRadius: 10,
        padding: 10,
        height: '100%',
        marginHorizontal: 5,
    },
    wrapper: {
        height: '100%'
    },
    image: {
        position: 'absolute',
        bottom: 10,
        right: 10,
        width: 40,
        height: 40,
    },
    text: {
        marginBottom: 10,
        color: 'white',
        fontSize: 16
    }
});