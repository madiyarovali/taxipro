import React from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  RefreshControl,
  Dimensions,
  SafeAreaView,
  StatusBar,
  Platform,
} from 'react-native';
import services from './../../services';
import BottomNav from '../../components/navigations/bottom';

import NavBar from 'src/components/NavBar';
import Slider from './Slider';
import ProfileButton from './Buttons/Profile';
import BalanceButton from './Buttons/Balance';
import AddressButton from './Buttons/Address';
import SupportButton from './Buttons/Support';
import CashoutButton from 'src/screens/cashout/Button';
import PaymentButton from 'src/screens/payment/Button';
import BaigaButton from 'src/screens/baiga/Button';
import CompetitionButton from 'src/screens/competition/Button';

export default class HomeScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {},
            park: {},
            loading: true,
            entries: [],
        };
    }

    async setUser() {
        var user_exists = await services.profile.userExists();
        if (!user_exists) {
            await services.profile.setUser();
        }

        var user = await services.profile.getUser();
        var parks = await services.profile.getParks();
        var park = parks.find((park) => park.id == user.current_park_id);
        var entries = await services.profile.getMedias(park.park.id);
        var loading = false;
        this.setState({user, loading, park, entries});
    }

    async componentDidMount() {
        await this.setUser();
        const {navigation} = this.props;
        this.focusListener = navigation.addListener('didFocus', () => {
        this.setUser();
        });
    }

    async initData() {
        let result = await services.profile.setUser();

        var user = await services.profile.getUser();
        var parks = await services.profile.getParks();
        var park = parks.find((park) => park.id == user.current_park_id);

        this.setState({user, park});
    }

    componentWillUnmount() {
        this.focusListener.remove();
    }

    async refreshData () {
        this.setState({ loading: true });
        await this.setUser();
    }

    refresh () {
        return (
            <RefreshControl
                refreshing={this.state.loading}
                onRefresh={this.initData.bind(this)}
            />
        );
    }

    render() {
        return (
            <SafeAreaView style={styles.safe_area}>
                <StatusBar barStyle="dark-content"/>
                <NavBar navigation={this.props.navigation}/>
                <ScrollView
                    contentContainerStyle={styles.scrollView}
                    refreshControl={this.refresh()}
                >
                    <Slider navigation={this.props.navigation} entries={this.state.entries}/>
                    <View style={styles.wrapper}>
                        <ProfileButton
                            user={this.state.user}
                            park={this.state.park}
                            navigation={this.props.navigation}
                        />
                        <BalanceButton
                            onPress={() => this.refreshData()}
                            park={this.state.park}
                            navigation={this.props.navigation}
                        />
                        <View style={styles.row}>
                            <CashoutButton navigation={this.props.navigation}/>
                            <PaymentButton
                                navigation={this.props.navigation}
                                park={this.state.park}
                            />
                        </View>
                        <View style={styles.row}>
                            <AddressButton
                                navigation={this.props.navigation}
                                park={this.state.park}
                            />
                            <SupportButton
                                navigation={this.props.navigation}
                                park={this.state.park}
                            />
                        </View>
                        <View style={styles.row}>
                            <BaigaButton navigation={this.props.navigation} park={this.state.park}/>
                            {/* <CompetitionButton navigation={this.props.navigation}/> */}
                        </View>
                    </View>
                </ScrollView>
                <BottomNav 
                    page="home"
                    show_baiga={this.state.park.park && this.state.park.park.is_baiga_enabled}
                    navigation={this.props.navigation}
                />
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    safe_area: {
        flex: 1,
        backgroundColor: '#FFFFFF'
    },
    scroll: {
        backgroundColor: 'white',
        fontFamily: "HelveticaRegular",
        width: '100%',
    },
    row: {
        display: 'flex',
        flexDirection: 'row',
        marginVertical: 5,
        height: 120,
    },
    wrapper: {
        paddingLeft: 16,
        paddingRight: 16,
        paddingBottom: 120,
    },
});
