import React from 'react'
import { Text, View, StyleSheet, Image, FlatList, TouchableOpacity, Platform, StatusBar } from 'react-native';
import Loader from '../../components/loader'
import services from './../../services'
import NavBar from 'src/components/NavBar';

var globalStyles = require('../../styles/style')

const font_family = Platform.OS == 'ios' ? 'System' : 'HelveticaBold1';
export default class ParkScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            user: {},
            loading: true,
            parks: []
        };
    }

    async componentDidMount() {
        this.init();
    }

    async init () {
        var user_exists = await services.profile.userExists();
        if (!user_exists) {
            await services.profile.setUser();
        }

        var user = await services.profile.getUser();
        var parks = await services.profile.getParks();
        var loading = false;

        this.setState({ user, loading, parks });
    }

    async selectPark(id) {
        await services.profile.selectPark(id);
        this.props.navigation.navigate('Home');
    }

    removeUser() {
        services.user.removeUser();
        services.profile.removeUser();
        this.props.navigation.navigate('Login');
    }

    renderListHeader = () => {
        return (
            <View>
                <View style={{ ...styles.parkWrapper, ...globalStyles.topDistance }}>
                    <NavBar navigation={this.props.navigation}/>
                    <View style={styles.titleWrapper}>
                        <Text style={styles.title}>
                            Выберите таксопарк
                        </Text>
                        <Text style={styles.subtitle}>
                            Таксопарки, в которых Вы зарегистрированы:
                        </Text>
                    </View>
                </View>
            </View>
        );
    }

    refresh = () => {
        this.init();
    }

    renderListItem = ({ item }) => {
        return (
            <TouchableOpacity
                onPress={() => {
                    this.selectPark(item.id);
                }}>
                <View style={styles.listItem}>
                    <View style={styles.parkInfoWrapper}>
                        <Text style={styles.listItemValue}>{item.park_name}</Text>
                        <Text style={styles.listItemLabel}>{this.state.user.first_name} {this.state.user.last_name}</Text>
                    </View>
                    <View
                        style={styles.arrowWrapper}>
                        <Image
                            source={require('../../assets/icons/arrow.png')}
                        />
                    </View>
                </View>
            </TouchableOpacity>
        );
    }

    render() {
        return (
            <View style={globalStyles.view}>
                <StatusBar barStyle="dark-content"/>
                <View style={styles.mainBodyWrapper}>
                    <View style={styles.container}>
                        {
                            this.state.loading ?
                                <Loader />
                                : null
                        }
                        <FlatList
                            onRefresh={this.refresh}
                            refreshing={this.state.loading}
                            data={this.state.parks}
                            ListHeaderComponent={this.renderListHeader}
                            renderItem={this.renderListItem}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    titleWrapper: {
        alignItems: 'center'
    },
    actionsBtnWrapper: {
        paddingHorizontal: 16,
        flexDirection: 'row',
    },
    parkWrapper: {
        paddingLeft: 16,
        paddingRight: 16,
    },
    mainBodyWrapper: {
        flex: 1,
    },
    clearButton: {
        position: "absolute",
        right: 16,
        bottom: 16
    },
    title: {
        fontSize: 30,
        fontFamily: font_family,
        fontWeight: 'bold',
        marginTop: 24,
        marginBottom: 8
    },
    subtitle: {
        fontSize: 20,
        fontWeight: '400',
        fontFamily: font_family,
        marginBottom: 40,
        textAlign: 'center',
        color: 'gray'
    },
    container: {
        flex: 1,
        paddingTop: 22
    },
    listItemLabel: {
        fontSize: 12,
        color: '#A7A4A1'
    },
    listItemValue: {
        fontSize: 18
    },
    exitBtnWrapper: {
        flex: 1
    },
    listItem: {
        paddingVertical: 8,
        paddingHorizontal: 16,
        borderBottomColor: '#E2E2E2',
        borderBottomWidth: 1,
        flexDirection: 'row'
    },
    parkInfoWrapper: {
        flex: 1,
    },
    arrowWrapper: {
        paddingRight: 8,
        alignItems: 'center',
        justifyContent: 'center'
    },
    item: {
        padding: 10,
        fontSize: 18,
        height: 44,
    },

});