import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  FlatList,
  TouchableOpacity,
  Platform,
  SafeAreaView,
} from 'react-native';
import TopComponent from '../../components/buttons/top';
import NavBar from 'src/components/NavBar';
import Loader from '../../components/loader';
import services from './../../services';
import BottomNav from '../../components/navigations/bottom';

const font_family = Platform.OS == 'ios' ? 'System' : 'HelveticaBold1';
var globalStyles = require('../../styles/style');

export default class ProfileScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
      park: {},
      loading: true,
    };
  }

  async componentDidMount() {
    var user_exists = await services.profile.userExists();
    if (!user_exists) {
      await services.profile.setUser();
    }

    var user = await services.profile.getUser();
    var parks = await services.profile.getParks();
    var park = parks.find((park) => park.id == user.current_park_id);
    var loading = false;

    this.setState({user, loading, park});
  }

  renderListHeader = () => {
    return (
      <View style={styles.profileWrapper}>
        <View style={globalStyles.profileMain}>
          <Image
            borderRadius={45}
            style={{...globalStyles.avatar, ...styles.profileAvatar}}
            source={{uri: this.state.user.avatar}}
          />
          <View style={globalStyles.profileMainInfo}>
            <Text style={globalStyles.username}>
              {this.state.user.first_name}
            </Text>
            <Text style={styles.balanceLabel}>Баланс</Text>
            <Text style={styles.balance}>{this.state.park.balance} ₸</Text>
          </View>
        </View>
        <Text style={styles.title}>Информация</Text>
      </View>
    );
  };
  renderListItem = ({item}) => {
    return (
      <View style={styles.listItem}>
        <Text style={styles.listItemLabel}>{item.label}</Text>
        <Text style={styles.listItemValue}>{item.value}</Text>
      </View>
    );
  };

  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#FFFFFF'}}>
        {/* <TopComponent navigation={this.props.navigation}/> */}
        <NavBar navigation={this.props.navigation}/>
        <View style={{ ...globalStyles.view, ...globalStyles.topDistance }}>
          <FlatList
            data={[
              {
                label: 'Фамилия и Имя',
                value: `${this.state.user.first_name} ${this.state.user.last_name}`,
              },
              {
                label: 'Личный счет №1 (по терминалу)',
                value: this.state.user.personal_account,
              },
              {label: 'Автомобиль', value: this.state.user.car_license_plate},
              {label: 'Основной баланс', value: this.state.park.balance},
              {
                label: 'Заработок за сегодня',
                value: this.state.user.today_balance,
              },
              {
                label: 'Километраж за сегодня',
                value: this.state.user.today_mileage,
              },
            ]}
            ListHeaderComponent={this.renderListHeader}
            renderItem={this.renderListItem}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
        <BottomNav 
          page="profile"
          show_baiga={this.state.park.park && this.state.park.park.is_baiga_enabled}
          navigation={this.props.navigation}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  titleWrapper: {
    alignItems: 'center',
  },
  profileWrapper: {
    paddingLeft: 16,
    paddingRight: 16,
    paddingTop: 10
  },
  mainBodyWrapper: {
    flex: 1,
  },
  clearButton: {
    position: 'absolute',
    right: 16,
    bottom: 16
  },
  balanceLabel: {
    fontSize: 16,
    marginTop: 8,
    color: '#A7A4A1',
  },
  balance: {
    fontSize: 25,
    fontFamily: font_family,
    fontWeight: 'bold',
  },
  title: {
    fontSize: 20,
    fontFamily: font_family,
    fontWeight: 'bold',
    textAlign: 'left',
    marginTop: 24,
    marginBottom: 8,
  },
  profileAvatar: {
    marginRight: 16,
  },
  container: {
    flex: 1,
    paddingTop: 22,
  },
  listItemLabel: {
    fontSize: 12,
    color: '#A7A4A1',
  },
  listItemValue: {
    fontSize: 18,
  },
  listItem: {
    paddingVertical: 8,
    paddingHorizontal: 16,
    borderBottomColor: '#E2E2E2',
    borderBottomWidth: 1,
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
});
