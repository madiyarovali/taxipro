import React from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons'

export default class BalanceButtonComponent extends React.Component {
    constructor (props) {
        super(props);
    }

    render () {
        return (
            <View style={styles.wrapper}>
                <View style={styles.card}>
                    <Text style={styles.text}>
                        Мой баланс
                    </Text>
                    <View style={styles.content}>
                        <Image
                            source={require('src/assets/images/wallet.png')}
                            style={styles.image}
                        />
                        <View style={styles.text_wrapper}>
                            <TouchableOpacity
                                onPress={() => this.props.onPress()}
                                style={styles.refresh_button}
                            >
                                <Icon 
                                    name={"refresh-circle-outline"}
                                    size={35}
                                />
                            </TouchableOpacity>
                            <Text style={styles.balance}>
                                {this.props.park ? this.props.park.balance : 0} ₸
                            </Text>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    wrapper: {
        display: 'flex',
        flexDirection: 'row',
        marginVertical: 5,
        height: 110,
    },
    card: {
        backgroundColor: '#E4F7FF',
        flex: 1,
        borderRadius: 10,
        padding: 10,
        marginHorizontal: 5,
    },
    image: {
        position: 'absolute',
        bottom: -10,
        left: -10
    },
    text: {
        marginTop: 10,
        fontSize: 16,
        fontWeight: '600',
        textAlign: 'right'
    },
    content: {
        flexDirection: 'row',
        alignItems: 'center',
        display: 'flex',
        flex: 1,
    },
    text_wrapper: {
        alignItems: 'center',
        justifyContent: 'flex-end',
        marginBottom: 15,
        width: "100%",
        flexDirection: 'row'
    },
    balance: {
        color: '#212121',
        fontWeight: '700',
        fontSize: 32,
        textAlign: 'right'
    },
    refresh_button: {
        margin: 5
    }
});