import React from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image
} from 'react-native';
import services from 'src/services'

export default class AddressButtonComponent extends React.Component {
    constructor (props) {
        super(props);
    }

    async addresses() {
        var result = await services.profile.getAddress();
        var address = result.find(
            (address) => address.id == this.props.park.park.id,
        );
        this.props.navigation.navigate('Static', {
            html: address.address,
            title: address.park.title,
            image: '',
        });
    }

    render () {
        return (
            <TouchableOpacity
                onPress={() => this.addresses()}
                style={styles.button}
            >
                <View style={styles.wrapper}>
                    <Image
                        source={require('src/assets/images/taxi.png')}
                        style={styles.image}
                    />
                    <Text style={styles.text}>
                        Адреса {'\n'}офисов
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    button: {
        backgroundColor: '#FFCF38',
        flex: 1,
        borderRadius: 10,
        padding: 10,
        marginHorizontal: 5,
        // height: '100%'
    },
    wrapper: {
        height: '100%'
    },
    image: {
        position: 'absolute',
        bottom: 10,
        right: 10,
        width: 40,
        height: 40,
    },
    text: {
        marginBottom: 10,
        color: 'white',
        fontSize: 16
    }
});