import React from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image
} from 'react-native';

export default class ProfileButtonComponent extends React.Component {
    constructor (props) {
        super(props);
    }

    profile() {
        this.props.navigation.navigate('Profile');
    }

    render () {
        return (
            <View style={styles.wrapper}>
                <TouchableOpacity
                    onPress={() => this.profile()}
                    style={styles.button}
                >
                    <View>
                        <View style={styles.flex_column}>
                            <View style={styles.part}>
                                <Text style={styles.username}>
                                    {this.props.user.first_name} {this.props.user.last_name}
                                </Text>
                                <Text style={styles.park_name}>
                                    {this.props.park && this.props.park.park_name ? this.props.park.park_name : ''}
                                </Text>
                            </View>
                            <View style={styles.part}>
                                <Text style={styles.car_model}>
                                    { this.props.user.car_model || '' }
                                </Text>
                                {
                                    this.props.user.car_license_plate
                                    ? <Text style={styles.car_license_plate}>
                                        { this.props.user.car_license_plate }
                                    </Text>
                                    : null
                                }
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    wrapper: {
        display: 'flex',
        flexDirection: 'row',
        marginVertical: 5,
        // minHeight: 110,
    },
    button: {
        backgroundColor: 'white',
        flex: 1,
        borderRadius: 10,
        paddingVertical: 11,
        paddingHorizontal: 20,
        marginHorizontal: 5,
        borderColor: '#E5E5E5',
        borderWidth: 1,
        borderRadius: 10
    },
    flex_column: {
        flexDirection: 'column'
    },
    part: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
        minHeight: 30,
        alignItems: 'center'
    },
    username: {
        fontSize: 18,
        fontWeight: '700',
        marginBottom: 20
    },
    park_name: {
        fontSize: 16,
        color: 'rgba(33,33,33,0.5)',
        marginBottom: 20
    },
    car_model: {
        fontSize: 16,
    },
    car_license_plate: {
        fontSize: 18,
        fontWeight: '700',
        borderWidth: 1,
        borderColor: 'black',
        borderRadius: 5,
        paddingVertical: 7,
        paddingHorizontal: 12,
    },
});