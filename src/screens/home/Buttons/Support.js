import React from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    Linking
} from 'react-native';
import services from 'src/services'

export default class SupportButtonComponent extends React.Component {
    constructor (props) {
        super(props);
    }

    async support() {
        var result = await services.profile.getSupport();
        var address = result.find(
            (address) => address.id == this.props.park.park.id,
        );
        Linking.openURL(`tel:${address.phone}`);
    }

    render () {
        return (
            <TouchableOpacity
                onPress={() => this.support()}
                style={styles.button}
            >
                <View style={styles.wrapper}>
                    <Image
                        source={require('src/assets/images/phone.png')}
                        style={styles.image}
                    />
                    <Text style={styles.text}>
                    Служба {'\n'}поддержки
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    button: {
        backgroundColor: '#E2574C',
        flex: 1,
        borderRadius: 10,
        padding: 10,
        marginHorizontal: 5,
        height: '100%'
    },
    wrapper: {
        height: '100%'
    },
    image: {
        position: 'absolute',
        bottom: 10,
        right: 10,
        width: 40,
        height: 40,
    },
    text: {
        marginBottom: 10,
        color: 'white',
        fontSize: 16
    }
});