import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  Dimensions,
  TouchableOpacity,
  Image,
  SafeAreaView,
} from 'react-native';
import services from './../../services';
import ImageLoader from 'react-native-image-progress';
import {WebView} from 'react-native-webview';
import TopComponent from '../../components/buttons/top';

var globalStyles = require('../../styles/style');
var window = Dimensions.get('window');

export default class ProfileScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
      park: {},
      loading: true,
    };
  }

  removeUser() {
    services.user.removeUser();
    services.profile.removeUser();
    this.props.navigation.navigate('Login');
  }

  render() {
    let props = this.props.navigation.state.params;
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#FFFFFF'}}>
        <TopComponent navigation={this.props.navigation} />
        <View style={globalStyles.view}>
          {/* <View style={globalStyles.actionsBtnWrapper}>
            <View style={styles.exitBtnWrapper}>
              <TouchableOpacity
                onPress={() => {
                  this.removeUser();
                }}
                style={globalStyles.exitBtn}>
                <Image source={require('./../../assets/images/logout.png')} />
              </TouchableOpacity>
              <Text
                style={{
                  flex: 1,
                  width: '100%',
                  textAlign: 'center',
                  marginLeft: -30,
                }}>
                Такси.Про
              </Text>
            </View>
          </View> */}
          <View>
            {props.image ? (
              <View style={styles.slide}>
                <ImageLoader
                  source={{uri: `${services.common.url}${props.image}`}}
                  style={styles.slider_image}
                  resizeMode={'contain'}
                />
              </View>
            ) : (
              <View style={{height: 30}}></View>
            )}

            <View style={styles.wrapper}>
              <Text style={styles.title}>{props.title}</Text>
              <View style={{overflow: 'hidden', width: '100%', height: 410}}>
                <WebView
                  source={{
                    html:
                      '<meta name="viewport" content="width=device-width, initial-scale=1">' +
                      props.html,
                  }}
                  style={{opacity: 0.99}}
                />
              </View>
            </View>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  slider_image: {
    flex: 1,
    height: undefined,
    width: undefined,
  },
  slide: {
    width: window.width,
    height: 220,
  },
  exitBtnWrapper: {
    flexDirection: 'row',
    display: 'flex',
    flex: 1,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  wrapper: {
    paddingHorizontal: 16,
  },
});
