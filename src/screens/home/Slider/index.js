import React from 'react';
import {
    View,
    TouchableOpacity,
    StyleSheet,
    Dimensions
} from 'react-native';
import services from 'src/services';
import Carousel from 'react-native-snap-carousel';
import ImageLoader from 'react-native-image-progress';

export default class SliderComponent extends React.Component {
    constructor (props) {
        super(props);
    }

    static(item) {
        this.props.navigation.navigate('Static', {
            html: item.body,
            title: item.title,
            image: item.media.url,
        });
    }

    _renderItem = ({item, index}) => {
        return (
            <View style={styles.slide}>
                <TouchableOpacity
                    style={styles.slider_image}
                    onPress={() => this.static(item)}
                >
                    <ImageLoader
                        source={{uri: `${services.common.url}${item.media.url}`}}
                        style={styles.slider_image}
                        resizeMode={'contain'}
                    />
                </TouchableOpacity>
            </View>
        );
    };

    render () {
        let width = Dimensions.get('screen').width;
        if (this.props.entries && this.props.entries.length > 0) {
            return (
                <View>
                    <Carousel
                        ref={(c) => {
                            this._carousel = c;
                        }}
                        autoplayInterval={4000}
                        autoplay={true}
                        loop={true}
                        data={this.props.entries}
                        renderItem={this._renderItem}
                        sliderWidth={width}
                        itemWidth={width}
                    />
                </View>
            );
        }
        return null;
    }
}

const styles = new StyleSheet.create({
    slider_image: {
        flex: 1,
        height: undefined,
        width: undefined,
    },
    slide: {
        width: window.width,
        height: 210,
        marginBottom: 20
    },
});