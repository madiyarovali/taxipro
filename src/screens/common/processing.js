import React from 'react'
import { Text, View, StyleSheet, ActivityIndicator, Platform, BackHandler } from 'react-native'
var globalStyles = require('../../styles/style')
import services from './../../services'

const font_family = Platform.OS == 'ios' ? 'System' : 'HelveticaBold1';
export default class ProcessingScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            card_id: null,
            type: null,
            amount: 0
        };
    }
    
    async componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
        var card_id = this.props.navigation.state.params.card_id;
        var type = this.props.navigation.state.params.type;
        var amount = this.props.navigation.state.params.amount;
        var operation = this.props.navigation.state.params.operation;
        var title = this.props.navigation.state.params.title;
        var subtitle = this.props.navigation.state.params.subtitle;
        var account_id = this.props.navigation.state.params.account_id;
        var current_version = await services.profile.getVersion(Platform.OS);
        this.setState({ card_id, type, amount });
        var response_array = await services.payment.proceed({ type, card_id }, amount, operation, account_id, current_version);
        var response = response_array[0];
        if (response.hasOwnProperty('message') && (response.message.toLowerCase().includes('ошибка')) || response_array[1].status == 400) {
            this.props.navigation.state.params.onGoBack(response.message);
            this.props.navigation.goBack();
            return;
        }
        await services.profile.setUser();
        if (response.hasOwnProperty('message') && response.message.toLowerCase().includes('успешно')) {
            this.props.navigation.navigate('Success', { title, subtitle, response });
            return;
        } else {
            response = {
                withdraw: {
                    amount: amount,
                    created_at: new Date()
                }
            };
            this.props.navigation.navigate('Success', { title, subtitle, response });
            return;
        }
        return;
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    }

    onBackPress = () => {
        return true;
    }

    render() {
        return (
            <View style={globalStyles.view}>
                <View style={styles.mainBodyWrapper}>
                    <View style={styles.titleWrapper}>
                        <ActivityIndicator size="large" color="#fce100"/>
                        <Text style={styles.subtitle}>Идет обработка...</Text>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    cashoutHeader: {
        paddingHorizontal: 16,
        paddingVertical: 16
    },
    cashoutWrapper: {
        flex: 1,
        justifyContent: 'center'
    },
    titleWrapper: {
        alignItems: 'center',
        marginTop: 40
    },
    mainBodyWrapper: {
        flex: 1,
        justifyContent: 'center'
    },
    title: {
        fontSize: 20,
        fontFamily: font_family,
        fontWeight: 'bold',
        textAlign: 'left',
        marginTop: 24,
        marginBottom: 0
    },
    container: {
        flex: 1,
        justifyContent: "center",
    },
    horizontal: {
        justifyContent: "space-around",
        padding: 10
    },
    image: {
        marginBottom: 36
    },
    subtitle: {
        marginTop: 20,
        fontSize: 16
    }
});