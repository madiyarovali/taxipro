import React from 'react'
import { Text, View, StyleSheet, Image, Platform } from 'react-native'
import PrimaryButton from '../../components/buttons/primary'
import services from './../../services'
var globalStyles = require('../../styles/style')

const font_family = Platform.OS == 'ios' ? 'System' : 'HelveticaBold1';
export default class SuccessPaymentForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            title: '',
            subtitle: '',
            response: {
                payment: {
                    amount: 0,
                    created_at: '',
                    type: ''
                }
            }
        };
    }

    async componentDidMount() {
        var title = this.props.navigation.state.params.title;
        var subtitle = this.props.navigation.state.params.subtitle;
        var response = await this.props.navigation.state.params.response;
        console.log(response.hasOwnProperty('payment'));
        if (response.hasOwnProperty('payment')) {
            response.content = response.payment;
        }
        if (response.hasOwnProperty('withdraw')) {
            response.content = response.withdraw;
            response.content.type = 'transaction';
        }

        this.setState({ title, subtitle, response });
        await services.profile.setUser();
    }

    render() {
        return (
            <View style={globalStyles.view}>
                <View style={styles.mainBodyWrapper}>
                    <View style={styles.titleWrapper}>
                        <Image style={styles.image} source={require('../../assets/icons/thank.png')} />
                        <Text style={globalStyles.title}>{ this.state.title }</Text>
                        <Text style={styles.subtitle}>{ this.state.subtitle }</Text>
                        <View style={styles.details_wrapper}>
                            <View style={styles.list_details}>
                                <Text style={styles.details}>{ `Сумма:` }</Text>
                                <Text style={styles.details}>{ `${this.state.response.hasOwnProperty('content') ? this.state.response.content.amount.toFixed(2) : ''}тг` }</Text>
                            </View>
                            <View style={styles.list_details}>
                                <Text style={styles.details}>{ `Дата:` }</Text>
                                <Text style={styles.details}>{ `${(new Date(this.state.response.hasOwnProperty('content') ? this.state.response.content.created_at : '').toLocaleDateString('ru-RU'))}`}</Text>
                            </View>
                            <View style={styles.list_details}>
                                <Text style={styles.details}>{ `Услуга:` }</Text>
                                <Text style={styles.details}>{ `${this.state.response.hasOwnProperty('content') ? (this.state.response.content.type == 'cafe' ? 'Кафе' : (this.state.response.content.type == 'carwash' ? 'Автомойка' : 'Выплата')) : ''}`}</Text>
                            </View>
                        </View>
                    </View>
                </View>
                <PrimaryButton
                    onPress={() => {
                        this.props.navigation.navigate('Home');
                    }}
                    style={styles.primaryBtn}
                >
                    Вернуться
                </PrimaryButton>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    cashoutHeader: {
        paddingHorizontal: 16,
        paddingVertical: 16
    },
    cashoutWrapper: {
        flex: 1,
        justifyContent: 'center'
    },
    titleWrapper: {
        alignItems: 'center',
        marginTop: 40
    },
    mainBodyWrapper: {
        flex: 1,
        justifyContent: 'center'
    },
    title: {
        fontSize: 20,
        fontFamily: font_family,
        fontWeight: 'bold',
        textAlign: 'left',
        marginTop: 24,
        marginBottom: 0
    },
    container: {
        flex: 1,
        justifyContent: "center",
    },
    horizontal: {
        justifyContent: "space-around",
        padding: 10
    },
    image: {
        marginBottom: 36
    },
    subtitle: {
        fontSize: 16
    },
    details: {
        alignSelf: 'flex-start',
        fontSize: 14
    },
    details_wrapper: {
        width: 200,
        margin: 20
    },
    list_details: {
        width: 200,
        display: 'flex',
        flexDirection: 'row',
        borderBottomColor: 'gray',
        borderBottomWidth: 1,
        justifyContent: 'space-between',
        marginVertical: 5
    }
});