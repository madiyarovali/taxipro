import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  FlatList,
  Image,
  Platform,
  KeyboardAvoidingView,
  SafeAreaView,
} from 'react-native';
import PaymentButton from '../../components/buttons/payment';
import TopComponent from '../../components/buttons/top';
import Loader from '../../components/loader';
import services from './../../services';

const font_family = Platform.OS == 'ios' ? 'System' : 'HelveticaBold';
var globalStyles = require('../../styles/style');

export default class PaymentScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
      park: {},
      loading: true,
      history: [],
      page: 1,
      total_pages: 1,
    };
  }

  async componentDidMount() {
    var user = await services.profile.getUser();
    var parks = await services.profile.getParks();
    var park = parks.find((park) => park.id == user.current_park_id);
    this.setState({user, park});
    var history = (await services.payment.getHistory()).data;
    var total_pages = await services.payment.getHistoryPagesCount();
    var loading = false;
    this.setState({loading, history, total_pages});
  }

  async downloadMore() {
    if (this.state.total_pages == this.state.page) {
      return;
    }
    var page = this.state.page + 1;
    var loading = true;
    this.setState({page, loading});
    var history_temp = (await services.payment.getHistory(page)).data;
    var history = this.state.history;
    if (history_temp.length > 0) {
      history_temp.forEach((el) => {
        history.push(el);
      });
    }
    var loading = false;
    this.setState({loading, history});
  }

  form(type, title) {
    this.props.navigation.navigate('PaymentForm', {type, title});
  }

  renderListHeader = () => {
    return (
      <View>
        <View style={styles.mainBodyWrapper}>
          <Text style={styles.addressTitle}>
            Оплатить балансом можно в комплексе "НАВИГАТОР" по адресу
          </Text>
          <Text style={styles.address}>
            {' '}
            ул. Байзакова 78А (ниже ул. Гоголя)
          </Text>
          <View
            style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <Image
              style={styles.logo}
              source={require('../../assets/images/logo.png')}
            />
          </View>
          <Text style={styles.balance}>{this.state.park.balance} ₸</Text>
          <Text style={styles.balanceLabel}>Баланс</Text>
          <View style={styles.paymentBtns}>
            <View style={styles.paymentBtnWrapper}>
              <PaymentButton
                onPress={() => {
                  this.form('carwash', 'Оплата автомойки\n(ул. Байзакова 78А)');
                }}
                type="carwash"
                icon={require('../../assets/icons/car.png')}
                style={styles.paymentBtn}>
                Автомойка
              </PaymentButton>
            </View>
            <View style={styles.paymentBtnWrapper}>
              <PaymentButton
                onPress={() => {
                  this.form('cafe', 'Оплата кафе\n(ул. Байзакова 78А)');
                }}
                type="cafe"
                icon={require('../../assets/icons/cafe.png')}>
                Кафе
              </PaymentButton>
            </View>
          </View>
        </View>
        <Text style={styles.title}>История покупок</Text>
      </View>
    );
  };

  renderListItem = ({item}) => {
    return (
      <View style={styles.listItem}>
        <View style={styles.listItemInfo}>
          <Text style={styles.listItemName}>
            {item.type == 'carwash' ? 'Автомойка' : 'Кафе'}
          </Text>
          <Text style={styles.listItemDate}>
            {new Date(item.created_at).toLocaleDateString('ru-RU')}
          </Text>
        </View>
        <View style={styles.listItemAmountWrapper}>
          <Text style={styles.listItemAmount}>{item.amount} ₸</Text>
        </View>
      </View>
    );
  };

  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#FFFFFF'}}>
        <KeyboardAvoidingView
          behavior={Platform.OS == 'ios' ? 'padding' : ''}>
          <View style={globalStyles.view}>
            <TopComponent navigation={this.props.navigation} />
            {this.state.loading ? <Loader /> : null}
            <View style={styles.container}>
              <FlatList
                data={this.state.history}
                ListHeaderComponent={this.renderListHeader}
                renderItem={this.renderListItem}
                keyExtractor={(item, index) => index.toString()}
                onEndReached={() => {
                  this.downloadMore();
                }}
                onEndReachedThreshold={0.1}
              />
            </View>
          </View>
        </KeyboardAvoidingView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  addressTitle: {
    textAlign: 'center',
    marginTop: 20,
  },
  address: {
    textAlign: 'center',
    fontWeight: 'bold',
  },
  logo: {
    height: 75,
    flex: 1,
    aspectRatio: 2,
    resizeMode: 'contain',
    borderRadius: 12,
  },
  mainBodyWrapper: {
    paddingLeft: 16,
    paddingRight: 16,
    paddingTop: 16,
    paddingBottom: 80,
    alignItems: 'center',
    backgroundColor: '#FCE000',
    position: 'relative',
  },
  title: {
    fontSize: 20,
    fontFamily: font_family,
    fontWeight: 'bold',
    textAlign: 'left',
    marginTop: 64,
    marginLeft: 16,
    marginBottom: 8,
  },
  balanceLabel: {
    fontSize: 20,
    marginBottom: 30,
  },
  balance: {
    fontSize: 36,
    marginTop: 0,
    fontFamily: font_family,
    fontWeight: 'bold',
    color: '#212121',
  },
  container: {
    flex: 1,
    //paddingTop: 22,
  },
  primaryBtn: {
    marginLeft: 8,
    marginRight: 8,
    marginBottom: 8,
    marginTop: 8,
  },
  secondaryBtn: {
    marginLeft: 8,
    marginRight: 8,
    marginBottom: 8,
    marginTop: 8,
  },
  paymentBtns: {
    flexDirection: 'row',
    position: 'absolute',
    bottom: -50,
  },
  paymentBtn: {
    flex: 1,
  },
  paymentBtnWrapper: {
    flex: 1,
    alignItems: 'center',
    paddingHorizontal: 16,
  },
  listItemAmount: {
    fontWeight: 'bold',
    fontFamily: font_family,
    fontSize: 20,
  },
  listItem: {
    flexDirection: 'row',
    paddingVertical: 8,
    paddingHorizontal: 16,
    borderBottomWidth: 1,
    borderBottomColor: '#E2E2E2',
  },
  listItemAmountWrapper: {
    justifyContent: 'center',
  },
  listItemName: {
    fontSize: 14,
  },
  listItemDate: {
    color: '#A7A4A1',
  },
  listItemInfo: {
    flex: 1,
  },
  addressLable: {},
});
