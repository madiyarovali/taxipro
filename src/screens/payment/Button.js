import React from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    Platform,
    Alert,
    ToastAndroid
} from 'react-native';

export default class PaymentButtonComponent extends React.Component {
    constructor (props) {
        super(props);
    }

    payment() {
        if (this.props.park.park.city == 'Алматы') {
            this.props.navigation.navigate('Payment');
        } else {
            var msg = 'Данная функция доступна только для города Алматы';
            if (Platform.OS === 'android') {
                ToastAndroid.show(msg, ToastAndroid.SHORT)
            } else {
                Alert.alert(msg);
            }
        }
    }

    render () {
        return (
            <TouchableOpacity
                onPress={() => this.payment()}
                style={styles.button}
            >
                <View style={styles.wrapper}>
                    <Image
                        source={require('src/assets/images/cup.png')}
                        style={styles.image}
                    />
                    <Text style={styles.text}>
                        Оплата {'\n'}балансом
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    button: {
        backgroundColor: '#6BA4FF',
        flex: 1,
        borderRadius: 10,
        padding: 10,
        height: '100%',
        marginHorizontal: 5,
    },
    wrapper: {
        height: '100%'
    },
    image: {
        position: 'absolute',
        bottom: 10,
        right: 10,
        width: 40,
        height: 40,
    },
    text: {
        marginBottom: 10,
        color: 'white',
        fontSize: 16
    }
});