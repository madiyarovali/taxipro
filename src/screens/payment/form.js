import React from 'react'
import { Text, View, StyleSheet, Alert, KeyboardAvoidingView, Platform } from 'react-native'
import MoneyInput from '../../components/input/money'
import NavBar from 'src/components/NavBar'
import PrimaryButton from '../../components/buttons/primary'
import services from './../../services'
var globalStyles = require('../../styles/style')

const font_family = Platform.OS == 'ios' ? 'System' : 'HelveticaBold1';
export default class PaymentForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            title: '',
            type: '',
            amount: '0',
            park: {},
        };
    }

    async componentDidMount() {
        var type = this.props.navigation.state.params.type;
        var title = this.props.navigation.state.params.title;
        var user = await services.profile.getUser();
        var parks = await services.profile.getParks();
        var park = parks.find(park => park.id == user.current_park_id);
        this.setState({ type, title, park });
    }

    pay() {
        if (this.state.amount > 0) {
            this.props.navigation.navigate('Processing',
                {
                    type: this.state.type,
                    amount: this.state.amount,
                    operation: 'payments',
                    title: 'Спасибо',
                    subtitle: 'Оплата прошло успешно',
                    account_id: this.state.park.account_id,
                    onGoBack: (message) => this.showAlert(message)
                }
            );
        }
    }

    setAmount(text) {
        var number_value = parseInt(text.replace(/\s/g, '')) || 0;
        var balance = this.state.park.balance;
        if (balance < 0) {
            this.setState({ amount: 0 });
            return;
        }
        if (balance >= number_value) {
            this.setState({ amount: number_value });
        } else {
            this.setState({ amount: balance });
        }
    }

    showAlert = (message) => {
        Alert.alert(
            "Ошибка",
            message,
            [
                {
                    text: "Повторить",
                    onPress: () => console.log("Retry"),
                },
            ]
        );
    }

    render() {
        return (
            <KeyboardAvoidingView behavior={Platform.OS == "ios" ? "padding" : ""}>
                <View style={[globalStyles.view, {paddingTop: 50}]}>
                    <NavBar navigation={this.props.navigation}/>
                    <View style={styles.mainBodyWrapper}>
                        <View style={styles.titleWrapper}>
                            <Text style={globalStyles.title}>
                                { this.state.title }
                            </Text>
                        </View>
                        <View style={globalStyles.paymentWrapper}>
                            <Text style={globalStyles.cashoutLabel}>Введите сумму</Text>
                            <MoneyInput 
                                value={this.state.amount}
                                onChangeText={text => {
                                    this.setAmount(text)
                                }}
                            />
                        </View>
                        <PrimaryButton
                            onPress={() => {
                                this.pay();
                            }}
                            style={styles.primaryBtn}
                        >
                            Оплатить
                        </PrimaryButton>
                    </View>
                </View>
            </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({
    cashoutHeader: {
        paddingHorizontal: 16,
        paddingVertical: 16
    },
    cashoutWrapper: {
        flex: 1,
        justifyContent: 'center'
    },
    addressInput: {
        borderWidth: 1,
        borderRadius: 5,
        textAlign: 'center',
        fontSize: 24,
        marginTop: 20,
        borderColor: '#E2E2E2',
        paddingVertical: 4,
        fontWeight: 'bold',
        fontFamily: font_family,
        width: '100%'
    },
    titleWrapper: {
        alignItems: 'center',
        marginTop: 40
    },
    mainBodyWrapper: {
        flex: 1,
    },
    title: {
        fontSize: 20,
        fontFamily: font_family,
        fontWeight: 'bold',
        textAlign: 'left',
        marginTop: 24,
        marginBottom: 0
    },
    container: {
        flex: 1,
        justifyContent: "center",
    },
    horizontal: {
        justifyContent: "space-around",
        padding: 10
    }
});