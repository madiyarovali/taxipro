import React from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image
} from 'react-native';

export default class CompetitionButtonComponent extends React.Component {
    constructor (props) {
        super(props);
    }

    competition() {
        this.props.navigation.navigate('Competition');
    }

    render () {
        return (
            <TouchableOpacity
                onPress={() => this.competition()}
                style={styles.button}
            >
                <View style={styles.wrapper}>
                    <Image
                        source={require('src/assets/images/hyundai.png')}
                        style={styles.image}
                    />
                    <Text style={styles.text}>
                        Конкурс Hyundai!
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    button: {
        backgroundColor: '#614CE2',
        flex: 1,
        borderRadius: 10,
        padding: 10,
        height: '100%',
        marginHorizontal: 5,
    },
    wrapper: {
        height: '100%'
    },
    image: {
        position: 'absolute',
        bottom: 10,
        right: 10,
        width: 40,
        height: 40,
    },
    text: {
        marginBottom: 10,
        color: 'white',
        fontSize: 16
    }
});