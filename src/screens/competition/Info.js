import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  Dimensions,
  TouchableOpacity,
  Image,
  SafeAreaView,
} from 'react-native';
import services from '../../services';
import ImageLoader from 'react-native-image-progress';
import {WebView} from 'react-native-webview';
import TopComponent from '../../components/buttons/top';

var globalStyles = require('../../styles/style');
var window = Dimensions.get('window');

export default class CompetitionInfoScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
      park: {},
      loading: true,
    };
  }

  render() {
    let props = this.props.navigation.state.params;
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#FFFFFF'}}>
        <TopComponent navigation={this.props.navigation} />
        <View style={globalStyles.view}>
          
          <View>
            
            <View style={styles.wrapper}>
              <Text style={styles.title}>Конкурс Hyundai</Text>
              <View style={{overflow: 'hidden', width: '100%', height: 410}}>
                <WebView
                  source={{
                    html:
                      '<head><meta name="viewport" content="width=device-width, initial-scale=1"></head>' +
                      '<body><p style="font-family: Helvetica;font-size: 22px;"><strong>Конкурс "Hyundai"</strong>  проходит в таксопарке каждую неделю: с понедельника по воскресенье включительно. Выполняйте заказы, отслеживайте результаты и смотрите на какой приз вы претендуете на данный момент в режиме реального времени. Считаются заказы только от 1 км.<br /><br />Результаты обновляются каждый час.' + 
                      '<br /><br />Участвуйте и выигрывайте! 🏆</p></body>',
                  }}
                  style={{opacity: 0.99}}
                />
              </View>
            </View>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  slider_image: {
    flex: 1,
    height: undefined,
    width: undefined,
  },
  slide: {
    width: window.width,
    height: 220,
  },
  exitBtnWrapper: {
    flexDirection: 'row',
    display: 'flex',
    flex: 1,
  },
  title: {
    fontSize: 24,
    paddingLeft: 10,
    fontWeight: 'bold',
  },
  wrapper: {
    paddingHorizontal: 16,
  },
});
