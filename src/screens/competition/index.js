import React from 'react';
import {
    View,
    Text
} from 'react-native';
import NavBar from 'src/components/NavBar';
import Journey from './Journey';
import Coupons from './Coupons';
import TabHeader from 'src/components/Tab/Header';
import TabContent from 'src/components/Tab/Content';

export default class CompetitionScreen extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
            titles: [
                'Поездки',
                'Мои купоны',
            ],
            contents: [
                <Journey navigation={this.props.navigation}/>,
                <Coupons/>,
            ],
            active_index: 0
        };
    }

    switchTabs (active_index) {
        this.setState({ active_index });
    }

    render () {
        return (
            <View>
                <NavBar navigation={this.props.navigation}/>
                <TabHeader
                    titles={this.state.titles}
                    activeIndex={this.state.active_index}
                    switchTabs={(index) => this.switchTabs(index)}
                />
                <TabContent
                    contents={this.state.contents}
                    activeIndex={this.state.active_index}
                />
            </View>
        );
    }
}