import React from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    StyleSheet
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

export default class JourneyHeaderComponent extends React.Component {
    constructor (props) {
        super(props);
    }

    competitionInfo() {
        this.props.navigation.navigate('CompetitionInfo');
    }

    render () {
        return (
            <View style={styles.wrapper}>
                <Text style={styles.header}>
                    Мои поездки
                </Text>
                <TouchableOpacity
                    onPress={() => this.competitionInfo()}
                    style={styles.info_button}
                >
                    <Icon 
                        name={"information-circle-outline"}
                        size={20}
                        color="#BEC2C4"
                    />
                    <Text style={styles.info_text}>
                        что это?
                    </Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = new StyleSheet.create({
    wrapper: {
        paddingHorizontal: 20,
        display: 'flex',
        flexDirection: 'row',
        marginVertical: 20,
    },
    header: {
        fontSize: 28,
        fontWeight: '700'
    },
    info_button: {
        display: 'flex',
        flexDirection: 'row',
        borderWidth: 1,
        borderRadius: 5,
        borderColor: '#BEC2C4',
        alignSelf: 'flex-start',
        paddingHorizontal: 10,
        paddingVertical: 4,
        marginLeft: 10,
        marginTop: 6
    },
    info_text: {
        marginLeft: 5,
        color: '#BEC2C4'
    }
});