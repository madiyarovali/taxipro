import React from 'react';
import {
    View,
    FlatList
} from 'react-native';
import Item from './Item';
import services from 'src/services';
import moment from 'moment';
import 'moment-timezone';

export default class JourneyContentComponent extends React.Component {
    constructor (props) {
        super(props);
        moment.tz('Asia/Almaty');
        this.state = {
            journeys: null,
            loading: true
        };
    }

    async init() {
        try {
            var start = moment().subtract(4, 'days').format('YYYY-MM-DD');
            var end = moment().format('YYYY-MM-DD');
            var journeys = await services.competition.get(`/api/tickets/slots/${start}/${end}/`);
            // var journeys = await services.competition.get(`/api/tickets/slots`);
            var loading = false;
            this.setState({ journeys, loading });
        } catch(e) {
            console.error(e);
        }
    }

    async componentDidMount() {
        this.init();
    }

    renderItem ({ item }, self) {
        return (
            <Item
                journey={item}
                onSubmit={() => self.init()}
            />
        );
    }

    render () {
        return (
            <View>
                <FlatList
                    data={this.state.journeys}
                    renderItem={(data_item) => this.renderItem(data_item, this)}
                    keyExtractor={(item) => item.date}
                />
            </View>
        );
    }
}