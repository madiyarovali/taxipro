import React from 'react';
import {
    TouchableOpacity,
    Text,
    StyleSheet
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import * as Animatable from 'react-native-animatable';
import services from 'src/services';
import moment from 'moment';
import 'moment-timezone';

export default class GetCouponComponent extends React.Component {
    constructor (props) {
        super(props);
        moment.tz('Asia/Almaty');
        this.state = {
            loading: false
        };
    }

    async getCoupon () {
        if (this.props.isActive) {
            var date = this.props.date;
            this.setState({ loading: true });
            var coupon = await services.competition.post(`/api/tickets/generate/${date}`);
            this.setState({ loading: false });
            this.props.onSubmit();
        }
    }

    setActiveButton () {
        if (this.state.loading) {
            return {
                backgroundColor: '#FCE00040'
            };
        }
        if (this.props.isActive) {
            return {
                backgroundColor: '#FCE000'
            };
        }
        if (!this.props.isActive) {
            return {
                backgroundColor: '#EDECE8'
            };
        }    
    }

    setActiveButtonText () {
        if (this.props.isActive) {
            return {
                color: 'black'
            };
        }
        if (!this.props.isActive) {
            return {
                color: '#0000005e'
            };
        }   
    }

    render () {
        const AnimatableIcon = Animatable.createAnimatableComponent(Icon);
        return (
            <TouchableOpacity
                disabled={this.state.loading}
                onPress={() => this.getCoupon()}
                style={[styles.button, this.setActiveButton()]}
            >
                <Text style={[styles.text, this.setActiveButtonText()]}>
                {
                    this.state.loading
                    ? <AnimatableIcon
                        animation="pulse"
                        name={"ellipse-outline"}
                        size={20}
                        color="#BEC2C4"
                    />
                    : null
                }
                    Получить купон
                </Text>
            </TouchableOpacity>
        );
    }
}

const styles = new StyleSheet.create({
    button: {
        display: 'flex',
        alignSelf: 'flex-end',
        paddingHorizontal: 15,
        paddingVertical: 10,
        borderRadius: 10
    },
    text: {
        fontSize: 20,
        textAlign: 'center'
    }
});