import React from 'react';
import {
    View,
    Text,
    StyleSheet
} from 'react-native';
import moment from 'moment';
import 'moment/locale/ru';
import GetCoupon from './GetCoupon';
import CouponGot from './CouponGot';

export default class JourneyContentItemComponent extends React.Component {
    constructor (props) {
        super(props);
        moment.tz('Asia/Almaty');
        this.state = {
            date: moment(this.props.journey.date).locale('ru').format('D MMMM YYYY')
        };
    }

    update () {
        this.props.onSubmit()
    }

    render () {
        return (
            <View style={styles.wrapper}>
                <View style={styles.first_part}>
                    <Text style={styles.date}>
                        {this.state.date}
                    </Text>
                    <Text style={styles.orders_count}>
                        Количесто поездок: {this.props.journey.orders_count}
                    </Text>
                </View>
                <View style={styles.second_part}>
                    {
                        moment().subtract(1, 'days').isAfter(this.props.journey.date) &&
                        this.props.journey.availableTicketSlots == 0 &&
                        this.props.journey.tickets.length != 0 ?
                        <CouponGot isSkipped={false}/> :
                        moment().subtract(1, 'days').isAfter(this.props.journey.date) &&
                        this.props.journey.orders_count < 15 ?
                        <CouponGot isSkipped={true}/> :
                        this.props.journey.availableTicketSlots > 0 ?
                        <GetCoupon
                            date={this.props.journey.date}
                            isActive={true}
                            onSubmit={() => this.update()}
                        /> :
                        this.props.journey.availableTicketSlots == 0 ?
                        <GetCoupon isActive={false}/> :
                        null
                    }
                    {
                        moment().isBefore(this.props.journey.date) &&
                        15 - this.props.journey.orders_count % 15 > 0 ?
                        <Text style={styles.need}>Еще поездок: {15 - this.props.journey.orders_count % 15}</Text> :
                        null
                    }
                </View>
            </View>
        );
    }
}

const styles = new StyleSheet.create({
    wrapper: {
        paddingHorizontal: 20,
        display: 'flex',
        flexDirection: 'row',
        borderBottomColor: '#A7A4A1',
        borderBottomWidth: 1,
        paddingVertical: 20,
    },
    first_part: {
        display: 'flex',
        flexDirection: 'column',
        flex: 1
    },
    second_part: {
        display: 'flex',
        flexDirection: 'column',
        flex: 1
    },
    date: {
        fontSize: 24
    },
    orders_count: {
        fontSize: 16,
        color: '#A7A4A1',
    },
    need: {
        textAlign: 'right'
    }
});