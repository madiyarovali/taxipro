import React from 'react';
import {
    View,
    Text,
    StyleSheet
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

export default class CouponGotComponent extends React.Component {
    constructor (props) {
        super(props);
    }

    render () {
        return (
            <View style={styles.button}>
                {
                    !this.props.isSkipped ?
                    <Text style={styles.text}>
                        <Icon 
                            name={"checkmark-circle-sharp"}
                            size={20}
                            color="#BEC2C4"
                        />
                        Получено
                    </Text> :
                    <Text style={styles.text}>
                        <Icon 
                            name={"close-circle-sharp"}
                            size={20}
                            color="#BEC2C4"
                        />
                        Не получено
                    </Text>
                }
            </View>
        );
    }
}

const styles = new StyleSheet.create({
    button: {
        display: 'flex',
        alignSelf: 'flex-end',
        paddingHorizontal: 10,
        paddingVertical: 10,
        borderRadius: 10,
        backgroundColor: '#EDECE8'
    },
    text: {
        fontSize: 20,
        textAlign: 'center',
        color: '#0000005e'
    }
});