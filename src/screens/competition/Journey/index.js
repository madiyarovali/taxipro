import React from 'react';
import {
    View,
    StyleSheet
} from 'react-native';
import Header from './Header';
import Content from './Content'

export default class JourneyComponent extends React.Component {
    constructor (props) {
        super(props);
    }

    render () {
        return (
            <View style={styles.wrapper}>
                <Header navigation={this.props.navigation}/>
                <Content/>
            </View>
        );
    }
}

const styles = new StyleSheet.create({
    wrapper: {
        paddingTop: 10,
    }
});