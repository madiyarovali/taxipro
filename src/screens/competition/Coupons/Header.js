import React from 'react';
import {
    View,
    Text,
    StyleSheet
} from 'react-native';
import numberToWordsRu from 'number-to-words-ru';

export default class JourneyHeaderComponent extends React.Component {
    constructor (props) {
        super(props);
    }

    couponAmount () {
        return numberToWordsRu.convert(this.props.coupons.length, {
            currency: {
                currencyNameCases: [
                    "купон",
                    "купона",
                    "купонов"
                ],
                currencyNounGender: {
                    integer: 0,
                },
            },
            showNumberParts: {
                integer: true,
                fractional: false,
            },
            convertNumbertToWords: {
                integer: false,
            },
        })
    }

    render () {
        return (
            <View style={styles.wrapper}>
                <Text style={styles.header}>
                    Мои купоны
                </Text>
                <View style={styles.coupons_amount_wrapper}>
                    <Text style={styles.coupons_amount}>
                        {this.couponAmount()}
                    </Text>
                </View>
            </View>
        );
    }
}

const styles = new StyleSheet.create({
    wrapper: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginVertical: 20,
    },
    header: {
        fontSize: 28,
        fontWeight: '700'
    },
    info_button: {
        display: 'flex',
        flexDirection: 'row',
        borderWidth: 1,
        borderRadius: 5,
        borderColor: '#BEC2C4',
        alignSelf: 'flex-start',
        paddingHorizontal: 10,
        paddingVertical: 4,
        marginLeft: 10,
        marginTop: 6
    },
    info_text: {
        marginLeft: 5,
        color: '#BEC2C4'
    },
    coupons_amount_wrapper: {
        backgroundColor: '#27AE6020',
        paddingVertical: 7,
        paddingHorizontal: 10,
        borderRadius: 10
    },
    coupons_amount: {
        color: '#27AE60',
        fontWeight: '700',
        fontSize: 18
    }
});