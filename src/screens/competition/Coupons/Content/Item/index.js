import React from 'react';
import {
    View,
    Text,
    StyleSheet
} from 'react-native';
import moment from 'moment';
import 'moment/locale/ru';

export default class JourneyContentItemComponent extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
            date: moment(this.props.coupon.date).locale('ru').format('D MMMM YYYY')
        };
    }

    render () {
        return (
            <View style={styles.wrapper}>
                <View style={styles.border}>
                    <View style={styles.first_part}>
                        <Text style={styles.id}>№{this.props.coupon.id}</Text>
                        <Text style={styles.date}>{this.state.date}</Text>
                        <Text style={styles.name}>{this.props.driver.full_name}</Text>
                    </View>
                    <View style={styles.divider}/>
                    <View style={styles.second_part}>
                        <Text style={styles.number}>№{this.props.coupon.id}</Text>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = new StyleSheet.create({
    wrapper: {
        padding: 5,
        backgroundColor: '#FCE000',
        marginVertical: 10,
    },
    border: {
        borderColor: 'white',
        borderWidth: 1,
        display: 'flex',
        flexDirection: 'row',
        position: 'relative',
    },
    first_part: {
        paddingVertical: 20,
        paddingHorizontal: 30,
        display: 'flex',
        flex: 4,
        flexDirection: 'column',
    },
    second_part: {
        paddingVertical: 20,
        paddingHorizontal: 30,
        display: 'flex',
        flex: 1,
        flexDirection: 'column',
        transform: [{
            rotate: '-90deg'
        }],
        justifyContent: 'center',
        alignItems: 'center'
    },
    divider: {
        width: 0,
        height: '100%',
        position: 'absolute',
        top: 0,
        left: '70%',
        borderColor: 'white',
        borderWidth: 3,
        borderStyle: 'dashed',
        borderRadius: 1,
    },
    id: {
        fontSize: 30,
        fontWeight: '700'
    },
    date: {
        fontSize: 20,
        fontWeight: '600'
    },
    name: {
        fontSize: 18,
        fontWeight: '500'
    },
    number: {
        fontSize: 20,
        fontWeight: '600'
    }
});