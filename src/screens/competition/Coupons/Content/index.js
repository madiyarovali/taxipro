import React from 'react';
import {
    View,
    FlatList
} from 'react-native';
import Item from './Item';
import services from 'src/services';
import moment from 'moment';
import 'moment-timezone';

export default class JourneyContentComponent extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
            driver: null,
        };
    }

    async componentDidMount() {
        try {
            var driver = await services.profile.getUser();
 
            this.setState({ driver });
        } catch(e) {
            console.error(e);
        }
    }

    renderItem ({ item }, self) {
        let driver = self.state.driver;
        return (
            <Item coupon={item} driver={driver}/>
        );
    }

    render () {
        return (
            <View>
                <FlatList
                    data={this.props.coupons}
                    renderItem={(obj) => this.renderItem(obj, this)}
                    keyExtractor={(item) => item.date}
                />
            </View>
        );
    }
}