import React from 'react';
import {
    View,
    StyleSheet
} from 'react-native';
import services from 'src/services';
import Header from './Header';
import Content from './Content'

export default class CouponsComponent extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
            coupons: [],
            loading: true,
            next_page: 2
        };
    }

    async componentDidMount() {
        try {
            var response = await services.competition.get(`/api/tickets`);
            var coupons = response.data;
            var next_page = response.next_page_url;
            var loading = false;
 
            this.setState({ coupons, loading, next_page });
        } catch(e) {
            console.error(e);
        }
    }

    render () {
        return (
            <View style={styles.wrapper}>
                <Header coupons={this.state.coupons}/>
                <Content coupons={this.state.coupons}/>
            </View>
        );
    }
}

const styles = new StyleSheet.create({
    wrapper: {
        paddingTop: 10,
        paddingHorizontal: 20,
    }
});