import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  Alert,
  Platform,
  KeyboardAvoidingView,
  SafeAreaView,
} from 'react-native';
import PrimaryButton from '../../components/buttons/primary';
import {TextInputMask} from 'react-native-masked-text';
import services from './../../services';
import TopComponent from '../../components/buttons/top';

const font_family = Platform.OS == 'ios' ? 'System' : 'HelveticaBold1';

export default class CardForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pan: '',
      amount: '0',
    };
  }

  async componentDidMount() {
    var type = this.props.navigation.state.params.type;
    var title = this.props.navigation.state.params.title;
    var user = await services.profile.getUser();
    var parks = await services.profile.getParks();
    var park = parks.find((park) => park.id == user.current_park_id);
    this.setState({type, title, park});
  }

  addCard = async () => {
    if (this.state.pan.length > 15) {
      var loading = true;
      var pan = this.state.pan.replace(/ /g, '');
      this.setState({loading});
      var response = await services.cashout.saveDebitCard(pan);
      if (response.hasOwnProperty('brand')) {
        this.props.navigation.state.params.onGoBack(response);
        this.props.navigation.goBack();
      } else {
        this.showAlert(response.message);
      }
    }
  };

  showAlert = (message) => {
    Alert.alert('Ошибка', message, [
      {
        text: 'Повторить',
        onPress: () => console.log('Retry'),
      },
    ]);
  };

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }}> 
       <TopComponent navigation={this.props.navigation}/>
        <KeyboardAvoidingView
          behavior={Platform.OS == 'ios' ? 'padding' : ''}>
          <View style={styles.inputWrapper}>
           
            <View style={styles.cardInputWrapper}>
              <Text style={styles.title}>Добавление карты</Text>
              <Text style={[styles.label, { paddingBottom: 8 }]}>Номер Карты</Text>
              <TextInputMask
                type={'credit-card'}
                value={this.state.pan}
                style={styles.input}
                onChangeText={(pan) => {
                  this.setState({
                    pan,
                  });
                }}
              />
            </View>

            <PrimaryButton
              onPress={() => {
                this.addCard();
              }}
              style={styles.primaryBtn}>
              Сохранить
            </PrimaryButton>
          </View>
        </KeyboardAvoidingView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    fontSize: 24,
    fontFamily: font_family,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 30,
    marginTop: 10,
  },
  input: {
    fontSize: 20,
    borderWidth: 1,
    borderColor: '#E2E2E2',
    borderRadius: 5,
    textAlign: 'center',
    paddingVertical: 10,
  },
  label: {
    color: '#A7A4A1',
  },
  primaryBtn: {
    marginRight: 0,
    marginLeft: 0,
    marginTop: 10,
  },
  inputWrapper: {
    padding: 15,
  },

  cardInputWrapper: {},
  otherInputWrapper: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: 10,
  },
  holderInputWrapper: {
    flex: 2,
  },
  dateInputWrapper: {
    flex: 1,
    marginLeft: 10,
  },
});
