import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  FlatList,
  Platform,
  SafeAreaView,
} from 'react-native';
import PaymentButton from '../../components/buttons/payment';
import TopComponent from '../../components/buttons/top';
import Loader from '../../components/loader';
import services from './../../services';

const font_family = Platform.OS == 'ios' ? 'System' : 'HelveticaBold1';
var globalStyles = require('../../styles/style');

export default class HistoryScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
      park: {},
      loading: true,
      history: [],
      page: 1,
      total_pages: 1,
    };
  }

  async componentDidMount() {
    await this.initialData();
  }

  initialData = async () => {
    var user = await services.profile.getUser();
    var parks = await services.profile.getParks();
    var park = parks.find((park) => park.id == user.current_park_id);
    this.setState({user, park});
    var history = (await services.cashout.getHistory()).data;
    var total_pages = await services.cashout.getHistoryPagesCount();
    var loading = false;
    this.setState({loading, history, total_pages});
  };

  async downloadMore() {
    if (this.state.total_pages == this.state.page) {
      return;
    }
    var page = this.state.page + 1;
    var loading = true;
    this.setState({page, loading});
    var history_temp = (await services.cashout.getHistory(page)).data;
    var history = this.state.history;
    if (history_temp.length > 0) {
      history_temp.forEach((el) => {
        history.push(el);
      });
    }
    var loading = false;
    this.setState({loading, history});
  }

  form(type, title) {
    this.props.navigation.navigate('PaymentForm', {type, title});
  }

  renderListHeader = () => {
    return (
      <View>
        <Text style={styles.title}>История выплат</Text>
      </View>
    );
  };

  amountStyle = (state) => {
    if (state == 'Paid') {
      return {
        color: '#27AE60',
      };
    }
    if (state == 'Canceled') {
      return {
        color: '#EB5757',
      };
    }
    if (state == 'ProcessError') {
      return {
        color: '#EB5757',
      };
    }
    return {
      color: '#828282',
    };
  };

  renderListItem = ({item}) => {
    return (
      <View style={styles.listItem}>
        <View style={styles.listItemInfo}>
          <Text style={styles.listItemName}>
            {
              item.state == 'processed'
              ? 'Выплачено'
              : item.state == 'failed'
              ? 'Ошибка'
              : item.state == 'cancelled'
              ? 'Отменен'
              : item.state == 'pending'
              ? 'В процессе'
              : ''
            }
          </Text>
          <Text style={styles.listItemDate}>
            {new Date(item.created_at).toLocaleDateString('ru-RU')}
          </Text>
        </View>
        <View style={styles.listItemAmountWrapper}>
          <Text style={[styles.listItemAmount, this.amountStyle(item.state)]}>
            {item.amount} ₸
          </Text>
        </View>
      </View>
    );
  };

  onRefresh = async () => {
    await this.initialData();
  };

  render() {
    return (
      <SafeAreaView>
        <TopComponent navigation={this.props.navigation} />
        <View style={globalStyles.view}>
          {this.state.loading ? <Loader /> : null}
          <View style={styles.container}>
            <FlatList
              data={this.state.history}
              ListHeaderComponent={this.renderListHeader}
              renderItem={this.renderListItem}
              keyExtractor={(item, index) => index.toString()}
              onEndReached={() => {
                this.downloadMore();
              }}
              onEndReachedThreshold={0.1}
              onRefresh={() => this.onRefresh()}
              refreshing={this.state.loading}
            />
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  mainBodyWrapper: {
    paddingLeft: 16,
    paddingRight: 16,
    paddingTop: 16,
    paddingBottom: 80,
    alignItems: 'center',
    backgroundColor: '#FCE000',
    position: 'relative',
  },
  title: {
    fontSize: 20,
    fontFamily: font_family,
    fontWeight: 'bold',
    textAlign: 'left',
    marginTop: 8,
    marginLeft: 16,
    marginBottom: 24,
  },
  balanceLabel: {
    fontSize: 20,
    marginBottom: 24,
  },
  balance: {
    fontSize: 36,
    marginTop: 36,
    fontFamily: font_family,
    fontWeight: 'bold',
    color: '#212121',
  },
  container: {
    flex: 1,
    //paddingTop: 22,
  },
  primaryBtn: {
    marginLeft: 8,
    marginRight: 8,
    marginBottom: 8,
    marginTop: 8,
  },
  secondaryBtn: {
    marginLeft: 8,
    marginRight: 8,
    marginBottom: 8,
    marginTop: 8,
  },
  paymentBtns: {
    flexDirection: 'row',
    position: 'absolute',
    bottom: -50,
  },
  paymentBtn: {
    flex: 1,
  },
  paymentBtnWrapper: {
    flex: 1,
    alignItems: 'center',
    paddingHorizontal: 16,
  },
  listItemAmount: {
    fontWeight: 'bold',
    fontFamily: font_family,
    fontSize: 20,
  },
  listItem: {
    flexDirection: 'row',
    paddingVertical: 8,
    paddingHorizontal: 16,
    borderBottomWidth: 1,
    borderBottomColor: '#E2E2E2',
  },
  listItemAmountWrapper: {
    justifyContent: 'center',
  },
  listItemName: {
    fontSize: 14,
    fontWeight: '600',
  },
  listItemDate: {
    color: '#A7A4A1',
    fontSize: 10,
  },
  listItemInfo: {
    flex: 1,
  },
});
