import React from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image
} from 'react-native';

export default class CashoutButtonComponent extends React.Component {
    constructor (props) {
        super(props);
    }

    cashout() {
        this.props.navigation.navigate('Cashout');
    }

    render () {
        return (
            <TouchableOpacity
                onPress={() => this.cashout()}
                style={styles.button}
            >
                <View style={styles.wrapper}>
                    <Image
                        source={require('src/assets/images/wallet1.png')}
                        style={styles.image}
                    />
                    <Text style={styles.text}>
                        Вывод с {'\n'}баланса
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    button: {
        backgroundColor: '#27AE60',
        flex: 1,
        borderRadius: 10,
        padding: 10,
        height: '100%',
        marginHorizontal: 5,
    },
    wrapper: {
        height: '100%'
    },
    image: {
        position: 'absolute',
        bottom: 10,
        right: 10,
        width: 40,
        height: 33,
    },
    text: {
        marginBottom: 10,
        color: 'white',
        fontSize: 16
    }
});