import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  FlatList,
  Alert,
  KeyboardAvoidingView,
  Dimensions,
  Platform,
  SafeAreaView,
} from 'react-native';
import MoneyInput from '../../components/input/money';
import PrimaryButton from '../../components/buttons/primary';
import SecondaryButton from '../../components/buttons/secondary';
import Loader from '../../components/loader';
import Swipeout from 'react-native-swipeout';
import services from './../../services';
import TopComponent from '../../components/buttons/top';

const font_family = Platform.OS == 'ios' ? 'System' : 'HelveticaBold1';
var globalStyles = require('../../styles/style');

export default class ProfileScreen extends React.Component {
  constructor(props) {
    var GivenDate = '2020-12-17';
    var CurrentDate = new Date();
    GivenDate = new Date(GivenDate);
    super(props);
    this.forceUpdateHandler = this.forceUpdateHandler.bind(this);
    this.state = {
      amount: '0',
      cards: [],
      user: {},
      park: {},
      loading: true,
      showForm: false,
      pan: '',
      isModalVisible: false,
      lastCard: null,
      commission: CurrentDate > GivenDate ? 0 : 200,
      min_val: 100
    };
  }

  
  setAmount(text) {
    var number_value = parseInt(text.replace(/\s/g, '')) || 0;
    var balance = this.state.park.balance;
    if (parseInt(balance) < 0) {
      this.setState({amount: 0});
      return;
    }

    if (parseInt(balance) - (this.state.commission + this.state.min_val) >= number_value) {
      this.setState({amount: parseInt(number_value)});
    } else if (parseInt(balance) - (this.state.commission + this.state.min_val) > 0) {
      this.setState({amount: parseInt(balance) - (this.state.commission + this.state.min_val)});
    } else if (parseInt(balance) - (this.state.commission + this.state.min_val) < 0) {
      this.setState({amount: 0});
    }
  }

  async componentDidMount() {
    await this.initialData();
  }

  initialData = async () => {
    var cards = await services.cashout.getDebitCards();
    var user = await services.profile.getUser();
    var parks = await services.profile.getParks();
    var park = parks.find((park) => park.id == user.current_park_id);
    var loading = false;
    this.setState({cards, loading, user, park});
  };

  selectCard(id) {
    let amount = this.state.amount;
    if (
      this.state.park.balance > 0 &&
      amount >= this.state.min_val &&
      amount <= this.state.park.balance - (this.state.commission + this.state.min_val)
    ) {
      let lastCard = id;
      this.setState({lastCard});
    } else {
      this.showAlert(
        `Для дальнейшего поступления заказов рекомендуем оставить на балансе минимум 100 тенге. Минимальная сумма вывода ${this.state.min_val} тенге.`,
      );
    }
  }

  continueCashout = () => {
    let id = this.state.lastCard;
    let amount = this.state.amount;
    if (this.state.lastCard == null) {
      return Alert.alert('Вы не выбрали карту', '', [
        {
          text: 'Отмена',
          onPress: () => this.toggleModal(),
        },
      ]);
    }
    let card_a = this.state.cards.find((card_b) => card_b.id == id);
    return Alert.alert(
      `Вы уверены что хотите вывести на карту ${card_a.masked_pan}?`,
      '',
      [
        {
          text: 'Отмена',
          onPress: () => this.toggleModal(),
        },
        {
          text: 'Продолжить',
          onPress: () => {
            this.props.navigation.navigate('Processing', {
              card_id: id,
              amount: amount,
              operation: 'withdraws',
              title: 'Спасибо!',
              subtitle: 'Ваш запрос отправлен',
              account_id: this.state.park.account_id,
              onGoBack: (message) => this.showAlert(message),
            });
          },
        },
      ],
    );
  };

  showTransactions = () => {
    this.props.navigation.navigate('History');
  };

  showAlert = (message) => {
    Alert.alert('Ошибка', message, [
      {
        text: 'Повторить',
        onPress: () => console.log('Retry'),
      },
    ]);
  };

  renderListHeader = () => {
    return (
      <View>
        <TopComponent navigation={this.props.navigation} />
        <View style={styles.cashoutHeader}>
          <View style={globalStyles.cashoutWrapper}>
            <Text style={styles.balance}>Вывод с баланса</Text>
            <Text style={styles.cashoutLabel1}>
              Ваш баланс:{' '}
              <Text style={styles.cashoutLabel}>
                {this.state.park.balance} ₸
              </Text>
            </Text>
            <MoneyInput
              value={this.state.amount}
              onChangeText={(text) => {
                this.setAmount(text);
              }}
            />
            <Text style={styles.comission}>
              * Комиссия на вывод составляет {this.state.commission} тг
            </Text>
            <Text style={styles.comission1}>
              С баланса спишется {parseInt(this.state.amount) + this.state.commission} тг.
            </Text>
          </View>
          <Text style={styles.title}>Выберите карту</Text>
        </View>
      </View>
    );
  };

  toForm = () => {
    this.props.navigation.navigate('CardForm', {
      onGoBack: (method) => {
        let a = this.state.cards;
        a.unshift(method);
        if (a.length > 5) {
          a.pop();
        }
        this.setState({cards: a});
      },
    });
  };

  forceUpdateHandler = () => {
    this.forceUpdate();
  };

  toggleModal = () => {
    let lastCard = null;
    this.setState({lastCard});
  };

  bgColor = (id) => {
    return this.state.lastCard != null && this.state.lastCard == id
      ? '#EDECE8'
      : 'white';
  };

  renderListItem = ({item}) => {
    if ('text' in item)
      return (
        <View>
          <TouchableOpacity
            onPress={() => {
              this.toForm();
            }}>
            <View
              style={{
                flexDirection: 'row',
                marginLeft: 20,
                marginTop: 15,
                alignItems: 'center',
              }}>
              <Image
                style={{width: 20, height: 20, marginRight: 15}}
                source={require('../../assets/images/addcard.png')}
              />
              <Text>{item.text}</Text>
            </View>
          </TouchableOpacity>
        </View>
      );

    var swipeoutBtns = [
      {
        text: 'Удалить',
        onPress: async () => {
          var response = await services.cashout.deleteDebitCard(item.id);
          var cards = this.state.cards.filter((card) => card.id != item.id);
          if (cards.length < 5) {
            cards.push({
                text: 'Добавить новую карту',
            });
          }
          this.setState({cards});
        },
        backgroundColor: 'red',
      },
    ];

    let image =
      item.masked_pan[0] == 4
        ? require('../../assets/images/visa.png')
        : require('../../assets/images/mastercard.png');
    return (
      <Swipeout right={swipeoutBtns} backgroundColor={this.bgColor(item.id)}>
        <TouchableOpacity
          onPress={() => {
            this.selectCard(item.id);
          }}>
          <View style={styles.listItem}>
            <Image
              style={{width: 40, height: 40, marginRight: 10}}
              source={image}
            />
            <View style={styles.cardInfoWrapper}>
              <Text style={styles.listItemNumber}>{item.masked_pan}</Text>
            </View>
            <View style={styles.arrowWrapper}>
              <Image source={require('../../assets/icons/arrow.png')} />
            </View>
          </View>
        </TouchableOpacity>
      </Swipeout>
    );
  };

  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#FFFFFF'}}>
        <KeyboardAvoidingView
          behavior={Platform.OS == 'ios' ? 'padding' : ''}>
          <View style={globalStyles.view}>
            {this.state.loading ? <Loader /> : null}
            <View style={styles.mainBodyWrapper}>
              <View style={styles.container}>
                <FlatList
                  data={this.state.cards}
                  ListHeaderComponent={this.renderListHeader}
                  renderItem={this.renderListItem}
                  keyExtractor={(item, index) => index.toString()}
                  onRefresh={() => this.initialData()}
                  refreshing={this.state.loading}
                />
              </View>
            </View>
            <View>
              <PrimaryButton
                onPress={() => {
                  this.continueCashout();
                }}
                style={styles.primaryBtn}>
                Вывести деньги
              </PrimaryButton>
              <SecondaryButton
                onPress={() => {
                  this.showTransactions();
                }}
                style={styles.secondaryBtn}>
                Показать все транзакции
              </SecondaryButton>
            </View>
          </View>
        </KeyboardAvoidingView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  cashoutHeader: {
    paddingHorizontal: 16,
    paddingVertical: 16,
  },
  mainBodyWrapper: {
    flex: 1,
  },
  cashoutLabel1: {
    marginTop: 20,
    fontSize: 16,
    width: Dimensions.get('window').width - 30,
  },
  title: {
    fontSize: 20,
    fontFamily: font_family,
    fontWeight: 'bold',
    textAlign: 'left',
    marginTop: 24,
    marginBottom: 0,
  },
  container: {
    flex: 1,
   // paddingTop: 22,
  },
  listItemName: {
    fontSize: 12,
    color: '#A7A4A1',
  },
  listItemNumber: {
    fontSize: 14,
  },
  listItem: {
    paddingVertical: 8,
    paddingHorizontal: 16,
    borderBottomColor: '#E2E2E2',
    borderBottomWidth: 1,
    flexDirection: 'row',
  },
  input: {
    fontSize: 20,
    borderWidth: 1,
    borderColor: '#E2E2E2',
    borderRadius: 5,
    textAlign: 'center',
  },
  label: {
    color: '#A7A4A1',
  },
  primaryBtn: {
    marginLeft: 8,
    marginRight: 8,
    marginBottom: 8,
    marginTop: 8,
  },
  comission1: {
    width: '100%',
    textAlign: 'left',
    color: 'black',
    fontSize: 14,
  },
  comission: {
    width: '100%',
    textAlign: 'left',
    color: 'red',
    fontSize: 14,
  },
  secondaryBtn: {
    marginLeft: 8,
    marginRight: 8,
    marginBottom: 8,
    marginTop: 8,
  },
  inputWrapper: {
    padding: 15,
  },
  cardImage: {
    marginRight: 16,
    width: 50,
  },
  cashoutLabel: {
    fontSize: 18,
    fontWeight: '600',
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
  cardInfoWrapper: {
    flex: 1,
    paddingTop: 11,
  },
  arrowWrapper: {
    paddingRight: 8,
    alignItems: 'center',
    justifyContent: 'center',
  },
  listItemText: {
    paddingHorizontal: 16,
    paddingVertical: 8,
    color: '#A7A4A1',
  },
  balance_wrapper: {
    flex: 1,
    width: 300,
  },
  balance: {
    fontSize: 36,
    marginTop: 0,
    fontFamily: font_family,
    fontWeight: 'bold',
    color: '#212121',
  },
});
