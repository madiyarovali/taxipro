import React from 'react'
import { Text, View, TextInput, TouchableOpacity, KeyboardAvoidingView, Image, Alert, StyleSheet, ScrollView } from 'react-native'
import { TextInputMask } from 'react-native-masked-text'
import PrimaryButton from '../../components/buttons/primary'
import SecondaryButton from '../../components/buttons/secondary'
import Loader from '../../components/loader'
import SplashScreen from 'react-native-splash-screen'
import Modal from 'react-native-modal'
import services from './../../services'

var globalStyles = require('../../styles/style')

export default class LoginScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            phone: '+7',
            name: '',
            loading: false,
            loading_btn: false
        };
        this.clearText = this.clearText.bind(this);
    }

    async componentDidMount() {
        SplashScreen.hide();
        var user_exists = await services.user.userExists();
        if (user_exists) {
            var pin_exists = await services.user.pinExists();
            this.props.navigation.navigate('PinLock', { user_exists, pin_exists });
        } else {
            this.setState({ loading: false });
        }
    }

    clearText() {
        this.setState({ phone: '+7' });
    }

    async validate() {
        this.setState({ loading_btn: true });
        var result = await services.user.sendCode(this.state.phone);
        if (!result.hasOwnProperty('message')) {
            this.props.navigation.navigate('Verify', { phone: this.state.phone });
            setTimeout(() => {
                this.setState({ loading_btn: false });
            }, 300000);
        } else {
            this.showAlert(result.message);
            this.setState({ loading_btn: false });
        }
    }

    async sendRequest () {
        let phone = this.state.phone;
        let name = this.state.name;
        let response = await services.user.sendRequest(phone, name);
        var isModalVisible = !this.state.isModalVisible;
        this.setState({ isModalVisible });
        return Alert.alert(
            "Успешно",
            "Ваша заявка отправлена",
            [
                {
                    text: "Готово",
                    onPress: () => console.log("Ok"),
                },
            ]
        );
    }

    requestName () {
        var isModalVisible = !this.state.isModalVisible;
        this.setState({ isModalVisible });
    }

    showAlert = (message) =>
        Alert.alert(
            "",
            "Желаете ли Вы зарегистрироваться в таксопарке iTaxi?",
            [
                {
                    text: "Отмена",
                    onPress: () => console.log("Retry"),
                },
                {
                    text: "Да",
                    onPress: () => this.requestName(),
                },
            ]
        );


    render() {
        return (
            <KeyboardAvoidingView behavior={Platform.OS == "ios" ? "padding" : ""}>
            <View style={globalStyles.view}>
                {
                    this.state.loading ?
                        <Loader/>
                    : null
                }
                <Modal isVisible={this.state.isModalVisible}>
                    <View style={styles.modal}>
                        <Text style={styles.modalTitle}>Номер телефона</Text>
                        <TextInput
                            style={styles.nameInput}
                            value={this.state.phone}
                            placeholder="Телефон"
                            onChangeText={text => {
                                this.setState({ phone: text })
                            }}/>
                        <Text style={styles.modalTitle}>Как к Вам обращаться?</Text>
                        <TextInput
                            style={styles.nameInput}
                            value={this.state.name}
                            placeholder="Имя"
                            onChangeText={text => {
                                this.setState({ name: text })
                            }}/>
                        <PrimaryButton
                            onPress={() => {
                                this.sendRequest();
                            }}
                            style={styles.primaryBtn}
                        >
                            Отправить заявку
                        </PrimaryButton>
                        <SecondaryButton
                            onPress={() => {
                                this.setState({ isModalVisible: false });
                            }}
                            style={styles.secondaryBtn}
                        >
                            Отменить
                        </SecondaryButton>
                    </View>
                </Modal>
                <View style={styles.titleWrapper}>
                    <Text style={globalStyles.title}>Вход</Text>
                </View>
                <View style={styles.phoneWrapper}>
                    <View>
                        <Text style={globalStyles.label}>Телефон</Text>
                        <TextInputMask
                            type={'custom'}
                            value={this.state.phone}
                            style={styles.input}
                            options={{
                                mask: '+7 (999) 999-9999'
                            }}
                            keyboardType='number-pad'
                            onChangeText={phone => {
                                this.setState({
                                    phone
                                })
                            }}
                        />
                    </View>
                    <TouchableOpacity
                        style={styles.clearButton}
                        onPress={this.clearText}
                    >
                        <Image
                            source={require('../../assets/icons/clear.png')}
                        />
                    </TouchableOpacity>
                </View>
                <PrimaryButton
                    loading={this.state.loading_btn}
                    onPress={() => {
                        this.validate();
                    }}
                    style={globalStyles.primaryBtn}
                >
                    Войти
                </PrimaryButton>
                <Text style={styles.rules}>
                    Нажимая “Войти” Вы даете согласие на обработку персональных данных
                </Text>
            </View>
            </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({
    modal: {
        backgroundColor: 'white',
        padding: 10
    },
    titleWrapper: {
        flex: 1,
        justifyContent:'center',
        alignItems: 'center'
    },
    phoneWrapper: {
        paddingLeft: 16,
        paddingRight: 16,
        paddingTop: 24,
        borderTopWidth: 1,
        borderTopColor: '#E2E2E2'
    },
    clearButton: {
        position: "absolute",
        right: 16,
        bottom: 16
    },
    input: {
        fontSize: 20,
        marginVertical: 10
    },
    rules: {
        color: '#A7A4A1',
        fontSize: 12,
        marginTop: 32,
        marginHorizontal: 16,
        marginBottom: 16,
        textAlign: 'center'
    },
    nameInput: {
        borderWidth: 1,
        borderRadius: 5,
        textAlign: 'center',
        fontSize: 24,
        marginVertical: 20,
        marginHorizontal: 10,
        borderColor: '#E2E2E2',
        paddingVertical: 4,
        fontFamily: 'Helvetica',
        // width: '100%'
    },
    modalTitle: {
        marginHorizontal: 10,
    },
});