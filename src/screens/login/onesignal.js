import OneSignal from 'react-native-onesignal';
import { Alert } from 'react-native';

export const initOneSignal = async () => {
    OneSignal.setAppId("14f3e303-389f-4745-8767-78c26c0b411e");
    OneSignal.setLogLevel(6, 0);

    OneSignal.promptForPushNotificationsWithUserResponse((response) => {});

    /* O N E S I G N A L  H A N D L E R S */
    OneSignal.setNotificationWillShowInForegroundHandler(
      (notifReceivedEvent) => {
        let notif = notifReceivedEvent.getNotification();

        const button1 = {
          text: 'Закрыть',
          onPress: () => {
            notifReceivedEvent.complete();
          },
          style: 'cancel',
        };

        const button2 = {
          text: 'Посмотреть',
          onPress: () => {
            // navigation.navigate('RequestPushScreen', {
            //   requestId: notif.notification.additionalData.apply,
            // });
            notifReceivedEvent.complete(notif);
          },
        };

        Alert.alert('', notif.title, [button1, button2], {
          cancelable: true,
        });
      },
    );
    OneSignal.setNotificationOpenedHandler((notification) => {
    //   navigation.navigate('RequestPushScreen', {
    //     requestId: notification.notification.additionalData.apply,
    //   });
    });
    OneSignal.setInAppMessageClickHandler((event) => {});
    OneSignal.addEmailSubscriptionObserver((event) => {});
    OneSignal.addSubscriptionObserver((event) => {});
    OneSignal.addPermissionObserver((event) => {});

    const deviceState = await OneSignal.getDeviceState();
    return deviceState;
}