import React from 'react'
import { Text, View, StyleSheet, Platform, Vibration, Animated, Image, Linking } from 'react-native'
import ReactNativePinView from "react-native-pin-view"
import Icon from 'react-native-vector-icons/Ionicons'
import FingerprintScanner from 'react-native-fingerprint-scanner'
import PrimaryButton from '../../components/buttons/primary'
import SecondaryButton from '../../components/buttons/secondary'
import services from './../../services'
import Modal from 'react-native-modal'
import VersionInfo from 'react-native-version-info'
import NavBar from 'src/components/NavBar'

const font_family = Platform.OS == 'ios' ? 'System' : 'HelveticaBold1';
var globalStyles = require('../../styles/style')
export default class PinLockScreen extends React.Component {
    constructor(props) {
        super(props)
        Icon.loadFont();
        this.state = {
            reenter_mode: false,
            enteredPin: '',
            checkPin: '',
            showCompletedButton: true,
            title: '',
            new_user: true,
            showRemoveButton: true,
            biometryType: '',
            errorMessageLegacy: undefined,
            biometricLegacy: undefined,
            isModalVisible: false
        };
        this.pinView = React.createRef()
        this.setEnteredPin = this.setEnteredPin.bind(this)
        this.setShowCompletedButton = this.setShowCompletedButton.bind(this)
        this.setShowRemoveButton = this.setShowRemoveButton.bind(this)
        this.shakeAnimation = new Animated.Value(0)
    }
    
    startShake() {
        Animated.sequence([
            Animated.timing(this.shakeAnimation, { toValue: 10, duration: 100, useNativeDriver: true }),
            Animated.timing(this.shakeAnimation, { toValue: -10, duration: 100, useNativeDriver: true }),
            Animated.timing(this.shakeAnimation, { toValue: 10, duration: 100, useNativeDriver: true }),
            Animated.timing(this.shakeAnimation, { toValue: 0, duration: 100, useNativeDriver: true })
        ]).start();
    }

    async setEnteredPin(value) {
        this.setState({ enteredPin: value });
        var pin_exists = await services.user.pinExists();
        if (value.length == 4 && pin_exists) { // for saved users
            var valid = await services.user.checkPin(value);
            if (valid) {
                this.props.navigation.navigate('Park');
            } else {
                Vibration.vibrate(200);
                this.startShake();
                this.pinView.current.clearAll();
                this.setState({ enteredPin: '' });
            }
        }
        if (value.length == 4 && !pin_exists && !this.state.reenter_mode) {  // for unsaved users to save first pin
            this.setState({
                enteredPin: '',
                checkPin: value,
                reenter_mode: true,
                title: 'Повторите код',
            });
            this.pinView.current.clearAll();
        } else if (value.length == 4 && !pin_exists && this.state.reenter_mode) {  // for unsaved users to check reentered pin
            if (this.state.checkPin == value) {
                services.user.setPin(value);
                this.props.navigation.navigate('Park');
            } else {
                Vibration.vibrate(200);
                this.startShake();
                this.pinView.current.clearAll();
            }
        }
    }

    setShowCompletedButton(value) {
        this.setState({ showCompletedButton: value })
    }

    setShowRemoveButton(value) {
        this.setState({ showRemoveButton: Value })
    }

    fingerPrintAuth() {
        if (Platform.OS == 'ios') {
            FingerprintScanner
                .authenticate({ description: 'Войти с помощью отпечатка пальца' })
                .then(() => {
                    this.onAuthenticate();
                })
                .catch((error) => {
                    this.setState({ errorMessageLegacy: error.message, biometricLegacy: error.biometric });
                });
        }
        if (Platform.OS == 'android') {
            if (this.requiresLegacyAuthentication()) {
                this.authLegacy();
            } else {
                this.authCurrent();
            }
        }
    }

    onAuthenticate() {
        this.props.navigation.navigate('Home');
    }

    componentWillUnmount = () => {
        FingerprintScanner.release();
    }

    requiresLegacyAuthentication() {
        return Platform.Version < 23;
    }

    authCurrent() {
        FingerprintScanner
            .authenticate({ title: 'Войти с помощью отпечатка пальца' })
            .then(() => {
                this.onAuthenticate();
            })
            .catch((error) => {
                this.setState({ errorMessageLegacy: error.message, biometricLegacy: error.biometric });
            });
    }

    authLegacy() {
        FingerprintScanner
            .authenticate({ onAttempt: this.handleAuthenticationAttemptedLegacy })
            .then(() => {
                this.onAuthenticate();
            })
            .catch((error) => {
                this.setState({ errorMessageLegacy: error.message, biometricLegacy: error.biometric });
            });
    }

    handleAuthenticationAttemptedLegacy(error) {
        this.setState({ errorMessageLegacy: error.message });
    };

    async componentDidMount() {
        let last_version = await services.profile.getVersion(Platform.OS);
        console.log(last_version,  VersionInfo.appVersion);
        if (parseFloat(VersionInfo.appVersion, 2) != parseFloat(last_version, 2)) {
            this.setState({ isModalVisible: true });
        }

        var user_exists = this.props.navigation.state.params.user_exists;
        var pin_exists = this.props.navigation.state.params.pin_exists;
        if (user_exists && pin_exists) {
            this.setState({
                title: 'Введите ранее заданный PIN',
                new_user: false
            });
        } else {
            this.setState({
                showCompletedButton: false,
                title: 'Придумайте код для быстрого входа',
                new_user: true
            });
        }
        FingerprintScanner
            .isSensorAvailable()
            .then(biometryType => this.setState({ biometryType }))
            .catch(error => this.setState({ errorMessage: error.message }));
        
    }

    renderRightButton() {
        if (this.state.showCompletedButton) {
            if (this.state.new_user && this.state.reenter_mode) {
                return (
                    <Icon name={"ios-refresh"} size={36} color={"black"} />
                );
            }
            if (!this.state.new_user) {
                let { biometryType } = this.state;
                if (this.state.biometryType == 'Touch ID' && this.biometric == 'Biometrics')
                    return (
                        <Icon name={"ios-finger-print"} size={36} color={"black"} />
                    );
                else
                    return (
                        <Image source={require('./../../assets/images/faceid.png')} />
                    );    
            }
            return null;
        }
        return null;
    }

    async updateApp() {
        if (Platform.OS == 'ios') {
            Linking.openURL('https://apps.apple.com/kz/app/%D1%82%D0%B0%D0%BA%D1%81%D0%B8-%D0%BF%D1%80%D0%BE/id1537979432');
        } else {
            Linking.openURL('https://play.google.com/store/apps/details?id=com.taxigroup');
        }
    }

    render() {
        return (
            <View style={{paddingTop: 30}}>
                <NavBar navigation={this.props.navigation}/>
                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                    <Modal isVisible={this.state.isModalVisible}>
                        <View style={styles.modal}>
                            <Text style={styles.modalTitle}>Доступно обновление</Text>
                            <View style={styles.modalButtonWrapper}>
                                <PrimaryButton
                                    onPress={() => {
                                        this.updateApp();
                                    }}
                                    style={styles.modalPrimaryButton}
                                >
                                    Обновить
                                </PrimaryButton>
                                <SecondaryButton
                                    onPress={() => {
                                        this.setState({ isModalVisible: false });
                                    }}
                                    style={styles.modalSecondaryButton}
                                >
                                    Отменить
                                </SecondaryButton>
                            </View>
                        </View>
                    </Modal>
                    <View style={styles.titleWrapper}>
                        <Text
                            style={styles.title}>
                            Быстрый вход
                        </Text>
                        <Text
                            style={styles.subtitle}>
                            { this.state.title }
                        </Text>
                    </View>
                    <Animated.View style={{ transform: [{ translateX: this.shakeAnimation }] }}>  
                        <ReactNativePinView
                            inputSize={32}
                            pinLength={4}
                            ref={this.pinView}
                            buttonSize={73}
                            onValueChange={value => this.setEnteredPin(value)}
                            buttonAreaStyle={{
                                marginTop: 24,
                            }}
                            inputAreaStyle={{
                                marginBottom: 24,
                            }}
                            inputViewEmptyStyle={{
                                backgroundColor: "#bec2c4",
                                borderWidth: 1,
                                width: 24,
                                height: 24,
                                borderColor: "transparent",
                            }}
                            inputViewFilledStyle={{
                                width: 24,
                                height: 24,
                                backgroundColor: "black",
                            }}
                            buttonViewStyle={{
                                borderWidth: 1,
                                borderColor: "#fce100",
                            }}
                            buttonTextStyle={{
                                color: "black",
                                fontSize: 35
                            }}
                            onButtonPress={key => {
                                if (key === "custom_left") {
                                    this.pinView.current.clear()
                                }
                                if (key === "custom_right") {
                                    if (this.state.new_user && this.state.reenter_mode) {
                                        this.setState({
                                            title: 'Придумайте код для быстрого входа',
                                            enteredPin: '',
                                            checkPin: '',
                                            reenter_mode: false,
                                        });
                                        this.pinView.current.clearAll();
                                    } 
                                    if (!this.state.new_user) {
                                        this.fingerPrintAuth();
                                    }
                                }
                            }}
                            customLeftButton={this.state.showRemoveButton ? <Icon name={"ios-backspace"} size={36} color={"black"} /> : undefined}
                            customRightButton={this.renderRightButton()}
                        />
                    </Animated.View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    modal: {
        backgroundColor: 'white',
        padding: 10,
    },
    modalButtonWrapper: {
        display: 'flex',
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'center',
        marginVertical: 10
    },
    modalPrimaryButton: {
        padding: 10
    },
    modalSecondaryButton: {
        padding: 10
    },
    titleWrapper: {
        paddingVertical: 40,
        justifyContent:'center',
        alignItems: 'center'
    },
    title: {
        fontSize: 28,
        fontFamily: font_family,
        fontWeight: 'bold',
        color: '#21201F'
    },
    subtitle: {
        fontSize: 16,
        marginTop: 10
    },
    pinWrapper: {
        flex: 1,
        alignItems: 'center'
    },
    resendBtn: {
        marginVertical: 20
    },
    modalTitle: {
        margin: 10,
        fontSize: 20,
        textAlign: 'center'
    },
    resendText: {
        color: '#A7A4A1'
    },
    primaryBtnWrapper: {
        marginBottom: 16
    }
});