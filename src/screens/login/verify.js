import React from 'react'
import { Text, View, StyleSheet, TouchableOpacity, Alert, KeyboardAvoidingView } from 'react-native';
import SmoothPinCodeInput from 'react-native-smooth-pincode-input';
import { initOneSignal } from 'src/screens/login/onesignal';
import PrimaryButton from '../../components/buttons/primary';
import services from './../../services';
import NavBar from 'src/components/NavBar';

var globalStyles = require('../../styles/style')

export default class VerifyScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            code: '',
            timer: 300,
        };
    }

    componentDidMount() {
        setInterval(() => {
            if (this.state.timer > 0) {
                this.setState({ timer: this.state.timer - 1 })
            } else {

            }
        }, 1000);
    }

    async checkCode() {
        var phone = this.props.navigation.state.params.phone;
        var device_state = await initOneSignal();
        var result = await services.user.checkCode(phone, this.state.code, device_state.userId);
        // console.log(result);
        if (!result.hasOwnProperty('exception') && !result.hasOwnProperty('errors')) {
            services.user.saveUser(phone);
            this.props.navigation.navigate('PinLock', { user_exists: false });
        } else {
            this.showAlert('Введенные данные неправильные');
        }
    }

    async sendCode() {
        var phone = this.props.navigation.state.params.phone;
        services.user.sendCode(phone);
        this.setState({ timer: 300 });
    }

    showAlert = (message) =>
        Alert.alert(
            "Ошибка",
            message,
            [
                {
                    text: "Повторить",
                    onPress: () => console.log("Retry"),
                },
            ]
        );

    render() {
        return (
            <View>
                <KeyboardAvoidingView behavior={Platform.OS == "ios" ? "padding" : ""}>
                <View style={{ ...globalStyles.view, ...globalStyles.center, ...globalStyles.topDistance }}>
                    <NavBar navigation={this.props.navigation}/>
                    <View style={styles.titleWrapper}>
                        <Text style={globalStyles.title}>Авторизация</Text>
                        <Text style={globalStyles.subtitle}>Код отправлен на {this.props.navigation.state.params.phone }</Text>
                    </View>
                    <View style={styles.pinWrapper}>
                        <SmoothPinCodeInput
                            autoFocus={true}
                            cellStyle={{
                                borderBottomWidth: 2,
                                borderColor: '#BEC2C4',
                            }}
                            cellSize={64}
                            cellStyleFocused={{
                                borderColor: '#FCE000',
                            }}
                            value={this.state.code}
                            onTextChange={code => this.setState({ code })}
                        />
                        {
                            this.state.timer == 0 ?
                                <TouchableOpacity
                                    onPress={() => {
                                        if (this.state.code.length == 4) {
                                            this.sendCode();
                                        }
                                    }}
                                    style={styles.resendBtn}>
                                    <Text style={styles.resendText}>Отправить снова</Text>
                                </TouchableOpacity>
                            : null
                        }
                        <Text style={styles.resendAfterText}>Повторная отправка доступна через { new Date(this.state.timer * 1000).toISOString().substr(14, 5) }</Text>
                    </View>
                    <View
                        style={styles.primaryBtnWrapper}
                        >
                        <PrimaryButton
                            onPress={() => {
                                this.checkCode();
                            }}
                            style={globalStyles.primaryBtn}
                        >
                            Продолжить
                        </PrimaryButton>
                    </View>
                </View>
                </KeyboardAvoidingView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    titleWrapper: {
        paddingVertical: 40,
        justifyContent:'center',
        alignItems: 'center'
    },
    pinWrapper: {
        flex: 1,
        alignItems: 'center'
    },
    resendBtn: {
        marginVertical: 20
    },
    resendText: {
        color: '#A7A4A1'
    },
    resendAfterText: {
        marginTop: 20
    },
    primaryBtnWrapper: {
        marginBottom: 16
    }
});