import React from 'react'

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Text, View, KeyboardAvoidingView, Platform } from 'react-native';

import LoginScreen from './screens/login/index'
import VerifyScreen from './screens/login/verify'
import PinLockScreen from './screens/login/lockscreen'
import HomeScreen from './screens/home/index'
import ProfileScreen from './screens/home/profile'
import CashoutScreen from './screens/cashout/index'
import PaymentScreen from './screens/payment/index'
import PaymentFormScreen from './screens/payment/form'
import SuccessPaymentForm from './screens/payment/success'


const Stack = createStackNavigator();

export default class AppContainer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isSignedIn: true
        };
    }
    render() {
        /* TODO: add navigation and change isSignedIn to token */
        return (
            <KeyboardAvoidingView behavior={Platform.OS == "ios" ? "padding" : "padding"}>
            <PinLockScreen />
            </KeyboardAvoidingView>
            // <NavigationContainer>
            //     {this.state.isSignedIn ? ( 
            //     <>
            //         <Stack.Screen name="Home" component={HomeScreen} />
            //     </>
            //     ) : (
            //     <>
            //         <Stack.Screen name="SignIn" component={LoginScreen} />
            //     </>
            //     )}
            // </NavigationContainer>
        );
    }
}