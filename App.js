import React from 'react'
import AppContainer from './src/navigation'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { initOneSignal } from 'src/screens/login/onesignal'

Icon.loadFont();
export default function App() {
    initOneSignal();
    return <AppContainer />
}